var timeChange = function() {
	console.log(timeslider.getValue());
	timeInterval = timeslider.getValue();
	redrawMapNodeLinkDiagram(timeInterval[0],timeInterval[1]);
	var hrs1 = Math.floor(timeInterval[0]*5/60);
	var mins1 = timeInterval[0]*5 - hrs1 * 60;
	var hrs2 = Math.floor(timeInterval[1]*5/60);
	var mins2 = timeInterval[1]*5 - hrs2 * 60;
	
	$('#timesliderlabel').text(hrs1 + ":" + mins1 + '-' + hrs2 + ":" + mins2);
};

var timeslider = $('.slider').slider({
	formater: function(value) {
		var hrs = Math.floor(value*5/60);
		var mins = value*5 - hrs * 60;
		return 'Time(' + hrs + ':' + mins + ')';
	}
	})
						
						 .on('slideStop', timeChange)
                         .data('slider');



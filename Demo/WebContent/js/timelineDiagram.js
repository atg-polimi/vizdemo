//timeline Diagram
/*
 * Description:
This is a day-based visualization of presence of a person inside different places at home 
in form of a timeline. Each place is represented with a colour and based on the time with a 
horizontal bar extending from left to right.
It is possible to separate the bars in case it's needed for a more clear view of the presence.
*/

//-----------------------------------------------------------------
//>>>TIMELINE PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var dislevel = [0,0,0,0];
var timelineStrech = 2;
var timeRange = [0, timelineStrech*288];

//-----------------------------------------------------------------
//>>>SVG PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var timelineDiagramMarginLeft = 20;
var timelineDiagramMarginRight = 20;
var timelineDiagramMarginBottom = 40;
var timelineDiagramMarginTop = 40;
var timelineDiagramWidth = timelineStrech*288 + timelineDiagramMarginLeft + timelineDiagramMarginRight;
var timelineDiagramHeight = 200;

//-----------------------------------------------------------------
//>>>Bar PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var barHeight = 20;
var barOpacity = 0.8;

//-----------------------------------------------------------------
//>>>INTERVAL SELECTION PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var intervalSelectOpacity = 0.2;

//-----------------------------------------------------------------
//>>>CREATE TIMELINE DIAGRAM>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var valueColors = [];
var givenDay = 1;
function createTimelineDiagram(day, colors) {
	valueColors = colors;
	givenDay = day;
	//console.log(dailyTimeline_data);
	var drag = d3.behavior.drag()
    			 .on("drag", function(d) {dragmove(this);})
    			 .on("dragstart", function(d) {dragstart(this);})
    			 .on("dragend", function(d) {dragend(this);});

	var svg = d3.select("#timeline")
				 .attr("width",timelineDiagramWidth)
				 .attr("height",timelineDiagramHeight)
				 .call(drag);
	
	svg.selectAll("rect")
	   .data(dailyTimeline_data)
	   .enter()
	   .append("rect")
	   .attr("class","timelinebar")
	   .attr("x", function(d) {return timelineStrech*(d.startId-1) + timelineDiagramMarginLeft;})
	   .attr("y", function(d) {return findDislevel(d.value)+timelineDiagramHeight/2;})
	   .attr("width",function(d) {return (timelineStrech*(d.endId-d.startId+1));})
	   .attr("height",barHeight)
	   .attr("opacity",barOpacity)
	   .style("fill",function(d) {return findColor(d.value);})
	   .on("mouseenter", function(d) { Log().add("onSegment", d); })
	   .on("mouseleave", function(d) { Log().add("outSegment", d); });
	
	//draw the time axis
	var x = d3.time.scale()
    .range([timelineDiagramMarginLeft, timelineStrech*288+timelineDiagramMarginLeft]);
	
	//time format
	var format = d3.time.format("%H:%M");
	
	//x.domain(d3.extent(dataset,function(d) {return d.id}));
	var startTime = format.parse("00:00");
	var endTime = format.parse("24:00");
	
	x.domain([startTime,endTime]);

	var xAxis = d3.svg.axis()
    .scale(x)
    .orient("top")
    .ticks(d3.time.hour,2)
    //.ticks(20);
    .tickFormat(d3.time.format("%H h"));

		
	svg.append("g")
     .attr("class", "x axis")
     //.attr("transform", "translate(0," + (timelineDiagramHeight - timelineDiagramMarginBottom) + ")")
     .attr("transform", "translate(0," + (timelineDiagramMarginTop) + ")")
     .attr("pointer-events","none")
     .call(xAxis)
	.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", timelineDiagramWidth/2)
    //.attr("y", 35)
    .attr("y", -25)
    .text("Time");
}

var timeInterval = [0,0];
//-----------------------------------------------------------------
//>>>REDRAW TIMELINE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var redrawTimeline = function() {
	//console.log(separationslider.getValue());
	var svg = d3.select("#timeline");
	
	dislevel[0] = -2*separationslider.getValue();
	for (i=1; i<dislevel.length; i++) {
		dislevel[i] = dislevel[i-1] + separationslider.getValue(); 
	}
	
	svg.selectAll(".timelinebar")
	   .remove();
	
	svg.selectAll(".timelinebar")
	   .data(dailyTimeline_data)
	   .enter()
	   .append("rect")
	   .attr("class","timelinebar")
	   .attr("x", function(d) {return timelineStrech*(d.startId-1) + timelineDiagramMarginLeft;})
	   .attr("y", function(d) {return findDislevel(d.value)+timelineDiagramHeight/2;})
	   .attr("width",function(d) {return (timelineStrech*(d.endId-d.startId+1));})
	   .attr("height",barHeight)
	   .attr("opacity",barOpacity)
	   .style("fill",function(d) {return findColor(d.value, valueColors);})
	   .on("mouseenter", function(d) { Log().add("onSegment", d); })
	   .on("mouseleave", function(d) { Log().add("outSegment", d); });
	
	Log().add("timeLineSlider", separationslider.getValue());
};

//-----------------------------------------------------------------
//>>>BAR SEPARATION SLIDER>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var separationslider = $('#separationslider').slider({
		tooltip:'hide'
	})
	 .on('slideStop', redrawTimeline)
     .data('slider');

//-----------------------------------------------------------------
//>>>UTILITY FUNCTIONS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
function findColor(place) {
	switch (place) {
	case "B":
		return valueColors[0];//"RGB(255,0,0)";
		break;
	case "K":
		return valueColors[1];//"RGB(255,255,0)";
		break;
	case "L":
		return valueColors[2];//"RGB(0,0,255)";
		break;
	case "T":
		return valueColors[3];//"RGB(0,255,0)";
		break;
	}
	return "RGB(255,255,0)";
}

function findDislevel(place) {
	switch (place) {
	case "B":
		return dislevel[0];
		break;
	case "T":
		return dislevel[1];
		break;
	case "L":
		return dislevel[2];
		break;
	case "K":
		return dislevel[3];
		break;
	}
	return 0;
}

function svg2timeId(x) {
	x = x - timelineDiagramMarginLeft;
	return (x * 288) / timeRange[1];
}

function timeId2time(x) {
	//hrs min calculation
	var format = d3.time.format("%H:%M");
	var hrs = Math.floor(x*5/60);
	var mins = x*5 - hrs * 60;
	var d = new Date(2000,1,1,hrs,mins,0,0);
	return format(d);
}
//-----------------------------------------------------------------
//>>>EVENTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------

function dragmove(d) {
	//console.log(d);
	var xy = d3.mouse(d);
	//console.log(xy);
	d3.select(".timeIntervalBox")
	  .attr("width",xy[0]-timeInterval[0]);
}

function dragstart(d) {
	//console.log(d);
	var xy = d3.mouse(d);
//	console.log(xy);
	timeInterval[0] = xy[0];
	d3.select(".timeIntervalBox")
	  .remove();
	d3.select(".timeIntervalBoxText")
	  .remove();
	d3.select("#timeline")
	  .append("rect")
	  .attr("class","timeIntervalBox")
	  .attr("x",xy[0])
	  //.attr("y",0)
	  .attr("y",timelineDiagramMarginTop)
	  .attr("width",0)
	  .attr("height",timelineDiagramHeight - timelineDiagramMarginBottom - timelineDiagramMarginTop)
	  .attr("opacity",intervalSelectOpacity);
}

function dragend(d) {
	//console.log("dragend");
	var xy = d3.mouse(d);
	if (xy[0] != timeInterval[0]) {
	d3.select("#timeline")
	  .append("text")
	  .attr("class","timeIntervalBoxText")
	  //.attr("x",xy[0]+5)
	  //.attr("y",timelineDiagramMarginTop)
	  .attr("x",timeInterval[0]+10)
	  .attr("y",timelineDiagramHeight-timelineDiagramMarginBottom+15)
	  .text(timeId2time(svg2timeId(timeInterval[0])) + " - " + timeId2time(svg2timeId(xy[0])));
	
	redrawMapNodeLinkDiagram(givenDay, Math.floor(svg2timeId(timeInterval[0])), Math.floor(svg2timeId(xy[0])));
//	console.log(Math.floor(svg2timeId(timeInterval[0])) + " " + Math.floor(svg2timeId(xy[0])));
	
	Log().add("timeLineSelection", [Math.floor(svg2timeId(timeInterval[0])), Math.floor(svg2timeId(xy[0]))]);
	}
}

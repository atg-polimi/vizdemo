/**
 * 
 */

function SelectionLabel(selected) {

	if (!(this instanceof SelectionLabel)) {
		return new SelectionLabel(selected);
	}

	this.selected = selected;
	this.text = "Selection " + this.selected.index;
	
	var	onclose = function() { Selection.remove(this.selected.index); };
	var self = this;
	
	var position = function() {	// immediate execution
		var startdate = self.selected.start;
		var enddate = self.selected.end;
		
		/*var odd = ((enddate - startdate) % 2) ? false : true;*/
		var half = Math.floor((enddate  - startdate) / 2);
		var middle = startdate + half;
		
		var angle = APP.x(getAngleOfDay(middle/*, odd*/));	// radians
		
		var MAX_RADIUS = general_config.radialTree.radius + general_config.radialTree.offset;
		var x = -MAX_RADIUS * Math.cos(angle);
		var y =  MAX_RADIUS * Math.sin(angle);
		
		// @see http://msdn.microsoft.com/en-us/library/ie/hh535760(v=vs.85).aspx - part I
		var M = document.getElementById("arc" + middle).getScreenCTM();
		
		var position = { top: (M.a*x + M.c*y + M.f) + PARAM.scroll.top,
					 	left: (M.b*x + M.d*y + M.e) + PARAM.scroll.left };
		
		// top & left offsets
		if (angle > Math.PI && angle <= 2*Math.PI)
			position.left -= 100;
		if (!(angle > Math.PI/2 && angle < 3*Math.PI/2)) 
			position.top -= 16;

		return position;	
	}();
	
	this.print = function() {
		var label = $("<div></div>")
			.attr("id", "label_" + self.selected.index)
			.attr("class", "selectionLabel")
			.css("top", position.top)
			.css("left", position.left)
			.css("z-index", 100)
			.css("background-color", general_config.selection.labelColor[self.selected.color-1]);
		
		var label_text = $("<div></div>")
			.css("width", "84%")
			.text(self.text);
		label.append(label_text);
		
		if ((self.selected.index % 1) === 0) {
			var label_close = $("<div></div>")
				.css("width", "16%")
				.css("height", "16px")
					.append($("<button></button>")
						.attr("type", "button")
						.attr("class", "close")
						.on("click", function() { return Selection.remove(self.selected.index); })
						.html("<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>"));	// bootstrap style
			
			label.append(label_close);
		}
		
		$("body").append(label);
	};
	
	SelectionLabel.prototype.remove = SelectionLabel.remove;
	
	return this;
}

SelectionLabel.remove = function(index) {
	$("#label_" + index).remove();
}

SelectionLabel.removeAll = function() {
	$(".selectionLabel").remove();
}
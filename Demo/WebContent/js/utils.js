/**
 * 
 */
// Convert from degrees to radians.
function toRadian(angle) {
	return angle * (Math.PI / 180);
}

// Convert from radians to degrees.
function toDegree(angle) {
	return angle * (180 / Math.PI);
}

function getChildren(d) {
	if (d.depth === general_config.dataset.depth)
		return [d.id];
	
	var res = [d.id];
	for(var i = 0; i < d.children.length; i++)
		$.merge(res, getChildren(d.children[i]));
	
	return res;
}
/******************************************************
 ************* (mostly) Selection part **************** 
******************************************************/
// Returns the referenced 'd'.
// It is fired by drag&drop events and is based on current mouse position.
function getSelectedArc(unit) {
	var	startAngle = computeAngleOnSelection(true);	// angle of the current selection
	var arc = d3.selectAll("path[id*=arc]").filter(function(d) { 
		return (startAngle >= getStartAngle(d)) && (startAngle <= getEndAngle(d)) && (d.depth === general_config.dataset.depth);
	})[0][0];	// ondrag arc

	return d3.select(arc).datum();
}

//Returns the angle relative to the current position of the mouse wrt the RadialMap reference point.
function computeAngleOnSelection(rad) {
	var position = $("#radialtree").position();
	// RadialTree center coordinates
	var MAX_RADIUS = general_config.radialTree.maxRadius + general_config.radialTree.offset;
	var xC = (MAX_RADIUS + position.left);
		yC = (MAX_RADIUS + position.top);
	
	// Reference point coordiantes (0°)
	var xA = (MAX_RADIUS + position.left);
		yA =  position.top;
	
	// User current selection position
	var xB = PARAM.mouse.x
		yB = PARAM.mouse.y;
	
	// Vectors' components
	var CA = [xA-xC, yA-yC]
		CB = [xB-xC, yB-yC];
	
	// Angle's between 2D vectors computation
	// @see http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/ 
	var dot = CA[0]*CB[0] + CA[1]*CB[1]
		det = CA[0]*CB[1] - CA[1]*CB[0];
	var angle = Math.atan2(det, dot) * (180 / Math.PI);  // [-180°; 180°]
	
	// Angle's correction
	if (angle < 0)
		angle += 360;
	
	// Optional conversion
	if (rad) return toRadian(angle);
	
	return angle;
}

// Given the 'id' reference, returns the related angle of the RadialMap.
function getAngleOfDay(day) {
	var size = 0;
	if (general_config.extremesAtZoom)
		size = general_config.extremesAtZoom.max - general_config.extremesAtZoom.min + 1;
	else
		size = 365;
	var domain = d3.range(size+1);
	for(var i = 0; i < domain.length; i++) domain[i]+= (general_config.extremesAtZoom) ? general_config.extremesAtZoom.min : 1;
	
	var scale = d3.scale.ordinal()
		.domain(domain)
		.rangePoints([0, general_config.radialTree.RadialBound]);
	
	return scale(day);
}

// Return the arcs belonging to the specified selection
function getSelectionArcs(selection) {
	return d3.selectAll("path[id*='arc']").filter(function(d) {
		return (d.id >= selection.start) && (d.id <= selection.end);
	});
}

function getStartAngle(d) {
	return Math.max(0, Math.min(general_config.radialTree.RadialBound, PARAM.x(d.x)));
}

function getEndAngle(d) {
	return Math.max(0, Math.min(general_config.radialTree.RadialBound, PARAM.x(d.x + d.dx)));
}

function getInnerRadius(d, rtm) {
	if (d.depth === 0) 
		return rtm.config.RadialTree.LAYER_SPACING[d.depth];
	
	var r = 0;
	var i = d.depth-1;
	while (i >= 0) {
		r += rtm.config.RadialTree.LAYER_SPACING[i] + rtm.config.RadialTree.LAYER_WIDTH[i];
		i--;
	}
	return r + rtm.config.RadialTree.LAYER_SPACING[d.depth];
}

function getOuterRadius(d, rtm) {
	if (d.depth === 0)
		return rtm.config.RadialTree.LAYER_SPACING[d.depth] + rtm.config.RadialTree.LAYER_WIDTH[d.depth];
	
	var r = 0;
	var i = d.depth - 1;
	while (i >= 0) {
		r += rtm.config.RadialTree.LAYER_SPACING[i] + rtm.config.RadialTree.LAYER_WIDTH[i];
		i--;
	}
	return r + rtm.config.RadialTree.LAYER_SPACING[d.depth] + rtm.config.RadialTree.LAYER_WIDTH[d.depth];
}
/***************************************************/

/***************************************************
************* (mostly) Zooming part ****************
***************************************************/
function getYInterpolate(depth, rtm) {	// TODO: to be improved 
	var length = general_config.dataset.depth + 1;
	var dy  = [rtm.config.RadialTree.LAYER_SPACING[0]];
	var dyr = [];
	
	for(var i = 1; i < depth; i++) {
		dyr.push(dy[i-1] + rtm.config.RadialTree.Tween_LAYERS_WIDTH);
		dy.push(dyr[i-1] + rtm.config.RadialTree.Tween_LAYERS_SPACE);
	}
	dyr.push(dy[depth-1] + rtm.config.RadialTree.Tween_LAYERS_WIDTH);
	
	var diff = rtm.config.RadialTree.DYDomain[depth-1] - dyr[depth-1];
	var inc = diff / (length-depth+1);
	
	dy.push(dyr[depth-1] + rtm.config.RadialTree.LAYER_SPACING[depth]);
	dyr.push(dy[depth] + rtm.config.RadialTree.LAYER_WIDTH[depth]);
	
	for(var j = depth; j < length-1; j++) {
		dy.push(dyr[j] + rtm.config.RadialTree.LAYER_SPACING[j+1]);
		dyr.push(dy[j+1] + rtm.config.RadialTree.LAYER_WIDTH[j+1] + inc);
	}

	dyr.splice(-1,1);
	dyr.push(general_config.radialTree.maxRadius);
	
	console.log(dy, dyr);
	return {yr: dy, dyr: dyr};
}

function getYInterpolateBrutal(depth, prefix) {	// TODO: to be replaced
	var dy = [];
	var dyr = [];
	if (depth === 1) {
		if (prefix === 'RT1') {
			dy  = [20, 82, 192, 305];
			dyr = [30, 132, 244, 357];
		} else {
			dy  = [30, 132, 244, 357];
			dyr = [40, 162, 275, 408];
		}
	} else if (depth === 2) {
		if (prefix === 'RT1') {
			dy  = [20, 60, 122, 244];
			dyr = [30, 70, 172, 344];
		} else {
			dy  = [30, 70, 172, 344];
			dyr = [40, 80, 202, 408];
		}
	} else if (depth === 3) {
		if (prefix === 'RT1') {
			dy  = [20, 60, 100, 170];
			dyr = [30, 70, 110, 310];
		} else {
			dy  = [30, 70, 110, 310];
			dyr = [40, 80, 120, 408];
		}
	}
	
	return {yr: dy, dyr: dyr};
}

var getZoomExtremes = function(d) {
	var first = d,
		last = d;
	
	while (first.children && last.children) {
		first = first.children[0];
		last = last.children[last.children.length-1];
	}
	
	var min = first.id;
	var max = last.id;
	
	return {min: min, max: max};
};

var refinedSelection = function(extremes) {
	var min = extremes.min;
	var max = extremes.max;
	
	var selectionsZoom = PARAM.getSelectionCopy();//rtm.getSelectionCopy();
	$.each(selectionsZoom, function(i, v) {
		v.status = "";
		if ((v.end < min) || (v.start > max))		// entirely before/after the zoom interval.
			v.status = 'hide';
		else if ((v.start >= min) && (v.end <= max))	// entirely contained into the zoom interval.
			v.status = 'visible';
		else {
			if ((v.start < min) && (v.end > max)) { 			// contains the zoom interval.
				selectionsZoom.push({start: min, end: max, index: v.index+0.2, color: v.color, status: 'visible'});
				selectionsZoom.push({start: max+1, end: v.end, index: v.index+0.3, color: v.color, status: 'hide'});
				v.end = min-1;
				v.index += 0.1;
				v.status = 'hide';
			} else if ((v.start < min) && (v.end < max)) {		// unbalanced in the 'left'.
				selectionsZoom.push({start: min, end: v.end, index: v.index+0.2, color: v.color, status: 'visible'});
				v.end = min-1;
				v.index += 0.1;
				v.status = 'hide';
			} else if ((v.start > min) && (v.end > max)) {		// unbalanced on the 'right'.
				selectionsZoom.push({start: v.start, end: max, index: v.index+0.1, color: v.color, status: 'visible'});
				v.start = max+1;
				v.index += 0.2;
				v.status = 'hide';
			}
		}
	});
	
	selectionsZoom.sort(function(a,b) {
		if (a.index < b.index) return -1;
		else if (a.index > b.index) return 1;
		else return 0;
	})
	
	return selectionsZoom;
}
/***************************************************/

/***************************************************
* Call the zooming function for all the RadialTree *
***************************************************/
function caller(d, call) {
	for (var i = 0; i < instances.length; i++) {
		if (instances[i] !== call) {
			var f = d3.select("#" + instances[i].config.RadialTree.prefix + "arc" + d.id + "_");
			callee(f[0][0]);
		}
	}
}

function callee(element) {
	var event; // The custom event that will be created
	
	if (document.createEvent) {
	  event = document.createEvent("HTMLEvents");
	  event.initEvent("dblclick", true, true);
	} else {
	  event = document.createEventObject();
	  event.eventType = "dblclick";
	}
	
	event.eventName = "dblclick";
	event.haveBeenCalled = true; 
	
	if (document.createEvent)
		element.dispatchEvent(event);
	else
		element.fireEvent("on" + event.eventType, event);
}
/***************************************************/

/**********************************************
 * ************** Analytics part **************
 *********************************************/ 
function highlightSelection(index) {
	var selection = jsonPath(rtm.getSelection(), "$[?(@.index=='" + index + "')]")[0]
	d3.select("#arc"+selection.start+"_")
}

function highlightDays(days) {
	d3.selectAll("path[id*='arc']").filter(function(d) {
		return ($.inArray(d, days.similarDays) != -1); 
	}).attr("stroke", general_config.similarity.stokeOn);
	
	d3.selectAll("path[id*='arc']").filter(function(d) {
		return d.name == days.referenceDay.name; 
	}).style("fill", general_config.similarity.fill);
}

function dehighlightDays(days) {
	if (!(days.similarDays === undefined || days.referenceDay === undefined)) {
		d3.selectAll("path[id*='arc']").filter(function(d) {
			return ($.inArray(d, days.similarDays) != -1); 
		}).attr("stroke", null);
	
		var arcs = d3.selectAll("path[id*='arc']").filter(function(d) {
			return d.name == days.referenceDay.name; 
		}).style("fill", function(d) {
			var t = d3.select(this).attr("id").split("arc");
			return "url(\#" + t[0] + "radialGradient"+ t[1].substring(0,t[1].length-1) +")";
		});
		
		$(".APbody").css("visibility", "hidden");
	}
}

function dehighlightSimilar(similar) {
	d3.selectAll("path[id*='arc']").filter(function(d) {
		return ($.inArray(d, similar) != -1); 
	}).attr("stroke", null);
}

function clearSimilarity() {
	AnalyticsPanel.locked = false;
	dehighlightDays({referenceDay: AnalyticsPanel.referenceDay, similarDays: AnalyticsPanel.similarDays});
	$("#refday").html("------");
}
/************************************************************/
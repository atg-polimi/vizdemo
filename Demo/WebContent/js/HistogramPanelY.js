/**
 * 
 */

function HistogramPanelY(statistics, container) {
	
	if (!(this instanceof HistogramPanelY)) {
		return new HistogramPanelY(statistics, container);
	}
	
	var data = statistics
		.sort(function(a, b) {	// Order by index.
			if (a.index < b.index) return -1;
			else if (a.index > b.index) return 1;
			else if (a.index == b.index) return 0;
		});

	var div = $(container);
	
	var barWidth = 15;
	var barMargin = 2;
		
	this.draw = function() {
		if (data.length > 0) {
			var y = d3.scale.linear()
				.domain([0, 100]);
			
			for(var i = 0; i < APP.value.label.length; i++) {
				// room container
				var width = data.length * (barWidth + barMargin);
				var room = d3.select(container)
					.append("div")
					.attr("id", APP.value.id[i])
					.attr("class", "roomV")
					.style("width", width + "px");
				
				// barchart box
				var translation = (parseInt(room.style("width")) - width) / 2;
				var barchart = room.append("svg")
					.datum(APP.value.prefix+APP.indexdd(i+1))
					.attr("class", "barchartV")
					.attr("transform", function(d) { return "translate("+ translation +",0)"; });
					
				var height = parseInt(barchart.style("height"));
				y.range([height, 0]);
				
				// barchart & bar-value
				var bar = barchart.selectAll("g").data(data, function(d) { return d.index; });
				
				// new selections
				bar.enter().append("g")
						.attr("transform", function(d, j) { return "translate("+ (j*(barWidth + barMargin)) +",0)"; })
						.on("mouseover", function(d) { showDottedLine(d, this); })
						.on("mouseout", function(d) { hideDottodLine(); })
						.on("click", function(d) { highlightSelection(d.index); });
				
				bar.append("rect")
					.attr("y", function(d) { return y(d[APP.value.prefix+APP.indexdd(i+1)] * 100); })
				    .attr("height", function(d) { return height - y(d[APP.value.prefix+APP.indexdd(i+1)] * 100); })
				    .attr("width", barWidth)
				    .style("fill", function(d) { 
//				    	console.log(d);
				    	return APP.Selection.color[Math.floor(d.color-1)]; })
				    .style("opacity", function(d) {
						switch (d.status) {
						case 'visible':
							return 1;
							break;
						case 'hide':
							return 0.4;
							break;
						}
					});
				// old selections
				bar.exit().remove();

				// description box
				room.append("div")
					.attr("class", "descriptionV")
					.text(APP.value.label[i]);
			}
			
			// left axis
			var yAxis = d3.svg.axis()
				.scale(y)
				.orient("left")
				.ticks(5)
			
			var axis = d3.select(container)
				.insert("div", "div")
					.attr("class", "axissvgV")
				.append("svg")
				.append("g")
					.attr("class", "axisV")
					.attr("transform", "translate(25,5)")
					.call(yAxis)
					.append("text")
						.attr("x", "1.5em")
						.attr("y", 0)
						.attr("dy", "0.5em")
						.text("%");
		}
	};
	
	var showDottedLine = function(d, elem) {
		var svg = elem.parentNode
			text = APP.percentage(d[d3.select(svg).datum()]);
				//d3.round(d[d3.select(svg).datum()], 2) + "%";
		
		// line position
		var translate = d3.select(elem).attr("transform")
			matches = translate.match(/\d+,\d+/)[0].split(",");
		
		var x1 = parseInt(matches[0]) + (barWidth / 2)
			x2 = (data.length + 1) * (barWidth + barMargin);
			y  = parseInt(matches[1]) + parseFloat(d3.select(svg).style("height")) - parseFloat(d3.select(elem).select("rect").attr("height")) - 0.5
		// -------------

		var g = d3.select(svg)
				.append("svg:g")
					.attr("id", "dottedline");
		g.append("svg:line")
			.attr("x1", x1)
			.attr("x2", x2)
			.attr("y1", y)
			.attr("y2", y)
			.attr("stroke", "black")
			.attr("stroke-width", "0.5")
			.attr("stroke-dasharray", "5 2");
		g.append("svg:text")
			.attr("x", x2)
			.attr("y", y - 5)
			.attr("fill", "black")
			.text(text);
	};
	
	var hideDottodLine = function() {
		d3.select("#dottedline").remove();
	};
	
	HistogramPanelY.prototype.clear = HistogramPanelY.clear(div);
	
}

// Static method
HistogramPanelY.clear = function(container) {
	var element = container || $("#intervalspanel");
	element.empty();
}
/**
 * 
 */

function HistogramPanelX(graphs) {
	
	if (!(this instanceof HistogramPanelX)) {
		return new HistogramPanelX();
	}
	
	// Singleton - part I
	var self = this;

	// init()
	this.values = graphs;
	
	var barHeight = 20;
	var barMargin = 1;
	
	this.scale = d3.scale.linear();
	
	for(var i = 0; i < self.values.length; i++) {
		var index = 0;
		for(var j = self.values[i].config.value.visualize[0]; j <= self.values[i].config.value.visualize[1]; j++) {
			var value = d3.select("#intervalspanel")
				.append("div")
				.attr("id", self.values[i].config.value.id[index])
				.attr("class", "roomH");
			
			value.append("div")
				.attr("class", "descriptionH")
				.text(self.values[i].config.value.label[index]);
			
			var barchart = value.append("svg")
				.attr("class", "barchartH")
				.attr("height", "23px");
			index++;
		}
		d3.select("#intervalspanel")
			.append("br");
	}
	//-------
	
	this.draw = function(selection) {
		for(var i = 0; i < self.values.length; i++) {
			var data = getStatistics(selection, self.values[i])
				.sort(function(a, b) {	// Order by index.
					if (a.index < b.index) return -1;
					else if (a.index > b.index) return 1;
					else if (a.index == b.index) return 0;
				});
			
			if (self.values[i].config.value.type === "absolute")
				self.scale.domain([self.values[i].config.value.min, self.values[i].config.value.max]);
			else 
				self.scale.domain([0, 1]);
			
			var index = 0;
			for(var j = self.values[i].config.value.visualize[0]; j <= self.values[i].config.value.visualize[1]; j++) {
				var barchart = d3.select("#intervalspanel > #" + self.values[i].config.value.id[index] + " > svg")
					.attr("height", data.length * (barHeight + barMargin));
				
				var width = parseInt(barchart.style("width"));
				self.scale.range([0, width]);
				
				// barchart & bar-value
				var bar = barchart.selectAll("g")
					.data(data, function(d) { return d.index;});
							
				// new selections
				var val = self.values[i].config.value.prefix+PARAM.indexdd(j);
				var g = bar.enter().append("g");
				g.append("rect");
				g.append("text");
				
				bar.select("rect")
				    .attr("width", function(d) { return self.scale(d[val]); })
				    .attr("height", barHeight - 1)
				    .style("fill", function(d) { return general_config.selection.color[d.color-1]; })
				    .style("opacity", function(d) {
						switch (d.status) {
						case 'visible':
							return 1;
							break;
						case 'hide':
							return 0.4;
							break;
						}
					});
				
				bar.select("text")
				    .attr("x", function(d) { return self.scale(d[val]) + 4; })
				    .attr("y", barHeight / 2)
				    .attr("dy", "0.35em")
				    .text(function(d) { 
				    	var text = (self.values[i].config.value.type === 'percentage') ? PARAM.percentage(d[val]) : d3.round(d[val], 2);
				    	return text;
				    });
				
				bar.attr("transform", function(d, k) { return "translate(0," + (k * (barHeight + barMargin)) + ")"; });
				
				// old selections
				bar.exit()
					.remove();
				
				index++;
			}
		}
	};
	
	
	var getStatistics = function(selection, value) {
		var selections = selection;
		var statistics = [];
		for (var s = 0; s < selections.length; s++) {
			var query = "$";
			for(var d = 1; d < general_config.dataset.depth; d++)
				query += ".children[*]";
			query += ".children[?(@.id>=" + selections[s].start + " && @.id<=" + selections[s].end + ")]";
			
			var selection = jsonPath(data_structure, query);
			var result = {};
			for(var j = value.config.value.visualize[0]; j <= value.config.value.visualize[1]; j++) {
				result[value.config.value.prefix+PARAM.indexdd(j)] = 0.0;
			}
				
			$.each(selection, function(index) {	// for each day (object) in the interval (array)
				for(var j = value.config.value.visualize[0]; j <= value.config.value.visualize[1]; j++)
					result[value.config.value.prefix+PARAM.indexdd(j)] += selection[index][value.config.value.prefix+PARAM.indexdd(j)];		// sum to the aggregate value
			});
			
			for(var j = value.config.value.visualize[0]; j <= value.config.value.visualize[1]; j++) {
				result[value.config.value.prefix+PARAM.indexdd(j)] = result[value.config.value.prefix+PARAM.indexdd(j)] / selection.length;	// compute the averaged statistics
			}
			
			result["index"] = selections[s].index;
			result["color"] = selections[s].color;
			result["status"] = selections[s].status;
			
			statistics.push(result);
		}
		return statistics;
	}
	
	// Singleton - part II
	HistogramPanelX = function() {
		return self;
	};
	
}

// Static method
HistogramPanelX.clear = function(container) {
	var element = container || $("#intervalspanel");
	element.empty();
}
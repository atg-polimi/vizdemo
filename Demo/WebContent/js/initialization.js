//Initialization
//reading data from the server, running the visualizations

//-----------------------------------------------------------------
//>>>SERVER PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
//map nodelink diagram
var mapNodeLink_data_url = "http://alpine-carrier-727.appspot.com/getDataset";
var mapNodeLink_analytics_url = "http://alpine-carrier-727.appspot.com/getAnalytics";
var mapNodeLink_data;
var mapNodeLink_analytics;

//daily timeline diagram
var dailyTimeline_data_url = "http://alpine-carrier-727.appspot.com/getDuration";
var dailyTimeline_data;

//-----------------------------------------------------------------
//>>>SERVER CONNECTION FOR DATA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
//window.onload = function() {
function initNodeLink(day, colors) {	
	var param = "?d="+day;
	//map transition diagram
	$.ajax({
		url: mapNodeLink_data_url+param,
		type: 'GET',
		async: false,
		crossDomain: true,
		dataType: 'json',
		success: function (data) {
			mapNodeLink_data = data;
		},
		error:function(data,status,er) {
			console.log("error: "+data+" status: "+status+" er:"+er);
		}
	});
	
	$.ajax({
		url: mapNodeLink_analytics_url+param,
		type: 'GET',
		async: false,
		crossDomain: true,
		dataType: 'json',	
		success: function (data) {
			mapNodeLink_analytics = data;
		},
		error:function(data,status,er) {
			console.log("error: "+data+" status: "+status+" er:"+er);
		}
	});
	
	//daily timeline diagram
	$.ajax({
		url: dailyTimeline_data_url+param,
		type: 'GET',
		async: false,
		crossDomain: true,
		dataType: 'json',	
		success: function (data) {
			dailyTimeline_data = data;
		},
		error:function(data,status,er) {
			console.log("error: "+data+" status: "+status+" er:"+er);
		}
	});

	//running the visualizations
	createMapNodeLinkDiagram(day, colors);
	createTimelineDiagram(day, colors);
}

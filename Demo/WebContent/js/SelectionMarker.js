/**
 * 
 */
function SelectionMarker(selection) {

    var	points = 50, // # of interpolation points 
    	index = Math.floor(selection.index);	// for 'sub-selections' 
    	radius = general_config.radialTree.maxRadius + general_config.selection.markerOffset[index-1];
    
	var angle = d3.scale.linear()
    	.domain([0, points-1])
    	.range([getAngleOfDay(selection.start), getAngleOfDay(selection.end+1)]);

	var line = d3.svg.line.radial()
    	.interpolate("basis")
    	.tension(0)
    	.radius(radius)
    	.angle(function(d, i) { return angle(i); });

	d3.select("#"+general_config.radialTree.id)
		.append("g")
			.attr("id", "selectionMarker"+selection.index)
		.append("path").datum(d3.range(points))
			.attr("d", line)
			.attr("class", "selectionMarker")
			.style("stroke", function() { return general_config.selection.color[selection.color-1]; });
}

SelectionMarker.remove = function(index) {
	if (index === 'all')
		$(".selectionMarker").remove();
	else
		$("[id*='selectionMarker" + Math.floor(index) + "']").remove();
};
//nodelinkMapbased Diagram
/*
 * Description:
This is a day-based visualization of presence of a person inside different places at home 
with nodes representing those places (their sizes representing the total amount of time spent in
that room) and links representing a spatial transition between two places.
*/

//PARAMETERS

//-----------------------------------------------------------------
//>>>LINK(ARC) PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var arcwidth = 2;		//the thickness of the links
var arcextent = 2.5;	//the spatial range (occupied space) of the links 

//-----------------------------------------------------------------
//>>>SVG PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var w = window;
var e = document.documentElement;
var g = document.getElementsByTagName('body')[0];

var width = w.innerWidth || e.clientWidth || g.clientWidth;
var height = w.innerHeight|| e.clientHeight|| g.clientHeight;

var nodeLinkDiagramDrag = d3.behavior.drag()
							.on("drag", function(d) {nodelinkdragmove(this);})
							.on("dragstart", function(d) {nodelinkdragstart(this);})
							.on("dragend", function(d) {nodelinkdragend(this);});

var svg = d3.select("#mapNodeLinkDiagram")
	.call(nodeLinkDiagramDrag);



//-----------------------------------------------------------------
//>>>NODE PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var centers = [{node: "B",cx:230,cy:80}, {node: "K",cx:108,cy:315}, {node: "L",cx:70,cy:70}, {node: "T",cx:250,cy:330}];
//var NODE_COLORS = ["RGB(255,0,0)", "RGB(0,255,0)", "RGB(0,0,255)", "RGB(255,255,0)"];
var NODE_COLORS;
var nodes = [{id:"B",cx:230,cy:80,color:"RGB(255,0,0)"},
             {id:"K",cx:108,cy:315,color:"RGB(0,255,0)"},
             {id:"L",cx:70,cy:70,color:"RGB(0,0,255)"},
             {id:"T",cx:250,cy:330,color:"RGB(255,255,0)"}];
var nodesOpacity = 0.6;

var increase_factor = 1.5;
var maximumNodeRadius = 50;

var Bx = 230; var By=80;
var Kx = 108; var Ky=315;
var Lx = 70; var Ly=70;
var Tx = 250; var Ty=330;

var midBKx = (Bx+Kx)/2; var midBKy = (By+Ky)/2;
var midBLx = (Bx+Lx)/2; var midBLy = (By+Ly)/2;
var midBTx = (Bx+Tx)/2; var midBTy = (By+Ty)/2;
var midKLx = (Lx+Kx)/2; var midKLy = (Ly+Ky)/2;
var midKTx = (Tx+Kx)/2; var midKTy = (Ty+Ky)/2;
var midLTx = (Lx+Tx)/2; var midLTy = (Ly+Ty)/2;

var dBK = - (Kx-Bx) / (Ky-By);
var dBKlength = Math.sqrt(1+dBK*dBK);
var dBKx = 1/dBKlength; var dBKy = dBK/dBKlength;

var dBT = - (Tx-Bx) / (Ty-By);
var dBTlength = Math.sqrt(1+dBT*dBT);
var dBTx = 1/dBTlength; var dBTy = dBT/dBTlength;

var dBL = - (Lx-Bx) / (Ly-By);
var dBLlength = Math.sqrt(1+dBL*dBL);
var dBLx = 1/dBLlength; var dBLy = dBL/dBLlength;

var dKL = - (Kx-Lx) / (Ky-Ly);
var dKLlength = Math.sqrt(1+dKL*dKL);
var dKLx = 1/dKLlength; var dKLy = dKL/dKLlength;

var dKT = - (Kx-Tx) / (Ky-Ty);
var dKTlength = Math.sqrt(1+dKT*dKT);
var dKTx = 1/dKTlength; var dKTy = dKT/dKTlength;

var dLT = - (Lx-Tx) / (Ly-Ty);
var dLTlength = Math.sqrt(1+dLT*dLT);
var dLTx = 1/dLTlength; var dLTy = dLT/dLTlength;

//-----------------------------------------------------------------
//>>>MAP PARAMETERS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
var mapFile = "img/background.png";
var mapWidth = 500;
var mapHeight = 500;
var mapX = 0;
var mapY = 0;
var mapOpacity = 0.3;

//-----------------------------------------------------------------
//>>>MapNodeLink DIAGRAM CREATION>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
function createMapNodeLinkDiagram(day, colors) {
	var givenDay = day;
	NODE_COLORS = colors;//["RGB(255,0,0)", "RGB(0,255,0)", "RGB(0,0,255)", "RGB(255,255,0)"];
	var svg = d3.select("#mapNodeLinkDiagram")
				.attr("width", 500)
				.attr("height", 500);
	
//	// ---------------- SVG BORDER --------
//	svg.append("rect")
//	   .attr("x",0)
//	   .attr("y",0)
//	   .attr("width",mapWidth)
//	   .attr("height",mapHeight)
//	   .attr("fill","none")
//	   .attr("stroke","black");
	
	// ---------------- BACKGROUND IMAGE (MAP) --------
	svg.append("image")
	   .attr("xlink:href",mapFile)
	   .attr("class","mapNodeLinkBackground")
	   .attr("width",mapWidth)
	   .attr("height",mapHeight)
	   .attr("x",mapX)
	   .attr("y",mapY)
	   .attr("opacity",mapOpacity);

	// ---------------- NODES--------------------
	maxNodeData = findMaximumNodeSize(mapNodeLink_analytics.nodes);
	var nodesgroup = svg.append("g")
						.attr("id","nodes")
						.attr("opacity",nodesOpacity);
	var node = nodesgroup.selectAll("circle")
						 .data(mapNodeLink_analytics.nodes)
						 .enter();

	var g = node.append("svg:g");
	g.append("circle")
		.attr("cx", function(d) { return getCenter(d.type).x; })
		.attr("cy", function(d) { return getCenter(d.type).y; })
		.attr("r", function(d) { return NodeRadius(d,maxNodeData); })
		.attr("id", function(d) { return d.type; })
		.attr("stroke", "black")
		.attr("fill", function(d, i) { return NODE_COLORS[i]; })
		.attr("transform", "translate(100, 100)")
		.on("mouseenter", function(d) { Log().add("onNode", d); })
		.on("mouseleave", function(d) { Log().add("outNode", d); });
		//.call(NodeDragBehavior);
	// ------------------------------------
	
	// -------- TRANSITIONS ------------------
	var arcsgroup = svg.append("g")
						.attr("id","arcs");
	var arc = arcsgroup.selectAll("path")
		.data(mapNodeLink_data)
		.enter();
	
	var t = arc.insert("svg:g");			
	t.append("svg:path")
			.attr("d", function(d,i) { 
				//return drawArc(d.value, mapNodeLink_data[i+1].value, 2);
				if (i < mapNodeLink_data.length-1) {
					if (d.value != mapNodeLink_data[i+1].value)
					{
//						console.log(d.value + " " + mapNodeLink_data[i+1].value);
						return drawArc(d.value,mapNodeLink_data[i+1].value,d.id);
					}
				} 
			})
			.attr("id", function(d) { return d.id; })
			.style("stroke", function(d) { return arcStroke(d.id); })
			//.style("stroke","black")
			.attr("stroke-width", arcwidth)
			.attr("fill", "none")
			.attr("transform", "translate(100, 100)")
			.on("mouseover", function(d) { onArcOver(this.parentNode, d, this); })
			.on("mouseout", function(d) { onArcOut(this.parentNode); })
			.on("mouseenter", function(d) { Log().add("onLink", d); })
			.on("mouseleave", function(d) { Log().add("outLink", d); });

	// -------------------------------------
	
}

//-----------------------------------------------------------------
//>>>MapNodeLink DIAGRAM REDRAW>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
function redrawMapNodeLinkDiagram(day, start, end) {

	var svg = d3.select("#mapNodeLinkDiagram");
	
	$.ajax({
		url: mapNodeLink_analytics_url + "?startId="+start+"&endId="+end+"&d="+day,
		type: 'GET',
		async: false,
		crossDomain: true,
		dataType: 'json',	
		success: function (data) {
			mapNodeLink_analytics = data;
		},
		error:function(data,status,er) {
			console.log("error: "+data+" status: "+status+" er:"+er);
		}
	});
	
	// ---------------- NODES--------------------
	//find max node percentage
	maxNodeData = findMaximumNodeSize(mapNodeLink_analytics.nodes);
	
	//node update
	var nodesgroup = svg.select("#nodes");
	var node2 = nodesgroup.selectAll("circle")
	 					  .data(mapNodeLink_analytics.nodes)
							.attr("cx", function(d) { return getCenter(d.type).x; })
							.attr("cy", function(d) { return getCenter(d.type).y; })
							.attr("r", function(d) { return NodeRadius(d,maxNodeData); })
							.attr("id", function(d) { return d.type; })
							.attr("stroke", "black")
							.attr("fill", function(d, i) { return NODE_COLORS[i]; })
							.attr("transform", "translate(100, 100)"); 					 

	// ------------------------------------
	
	
	// -------- TRANSITIONS ------------------
	var arcsgroup = svg.select("#arcs");
	var arc = arcsgroup.selectAll("path")
					    .remove();
	
	var arc2 = arcsgroup.selectAll("path")
						 .data(mapNodeLink_data)
						 .enter();
	
	var t = arc2.insert("svg:g");			
	t.append("svg:path")
			.attr("d", function(d,i) { 
				//return drawArc(d.value, mapNodeLink_data[i+1].value, 2);
				if (i < mapNodeLink_data.length-1) {
					if ((d.value != mapNodeLink_data[i+1].value) & (d.id>=start) & (d.id<=end-1))
					{
						//console.log("redrawing some arcs.");
						return drawArc(d.value,mapNodeLink_data[i+1].value,d.id);
					}
				} 
			})
			.attr("id", function(d) { return d.id; })
			.style("stroke", function(d) { return arcStroke(d.id); })
			//.style("stroke","black")
			.attr("stroke-width", arcwidth)
			.attr("fill", "none")
			.attr("transform", "translate(100, 100)")
			.on("mouseover", function(d) { onArcOver(this.parentNode, d, this); })
			.on("mouseout", function(d) { onArcOut(this.parentNode); });

	// -------------------------------------
	
}

//-----------------------------------------------------------------
//>>>UTILITY FUNCTIONS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
function drawArc(from, to, timeid) {

	var midx = 0;
	var midy = 0;

	switch (from) {
		case "L":
			fromx = Lx;
			fromy = Ly;
			break;
		case "B":
			fromx = Bx;
			fromy = By;
			break;
		case "T":
			fromx = Tx;
			fromy = Ty;
			break;
		case "K":
			fromx = Kx;
			fromy = Ky;
			break;
	}
	
	switch (to) {
	case "L":
		tox = Lx;
		toy = Ly;
		break;
	case "B":
		tox = Bx;
		toy = By;
		break;
	case "T":
		tox = Tx;
		toy = Ty;
		break;
	case "K":
		tox = Kx;
		toy = Ky;
		break;
	}
	
	if (((from === "B") & (to === "L")) | ((from === "L") & (to === "B"))) {
		midx = midBLx;
		midy = midBLy;
		midx += dBLx*(timeid-288/2)/arcextent; 
		midy += dBLy*(timeid-288/2)/arcextent; 
	}
	if (((from === "B") & (to === "T")) | ((from === "T") & (to === "B"))) {
		midx = midBTx;
		midy = midBTy;
		midx += dBTx*(timeid-288/2)/arcextent; 
		midy += dBTy*(timeid-288/2)/arcextent; 
	}
	if (((from === "B") & (to === "K")) | ((from === "K") & (to === "B"))) {
		midx = midBKx;
		midy = midBKy;
		midx += dBKx*(timeid-288/2)/arcextent; 
		midy += dBKy*(timeid-288/2)/arcextent; 
	}
	if (((from === "T") & (to === "L")) | ((from === "L") & (to === "T"))) {
		midx = midLTx;
		midy = midLTy;
		midx += dLTx*(timeid-288/2)/arcextent; 
		midy += dLTy*(timeid-288/2)/arcextent; 
	}
	if (((from === "T") & (to === "K")) | ((from === "K") & (to === "T"))) {
		midx = midKTx;
		midy = midKTy;
		midx += dKTx*(timeid-288/2)/arcextent; 
		midy += dKTy*(timeid-288/2)/arcextent; 
	}
	if (((from === "K") & (to === "L")) | ((from === "L") & (to === "K"))) {
		midx = midKLx;
		midy = midKLy;
		midx += dKLx*(timeid-288/2)/arcextent; 
		midy += dKLy*(timeid-288/2)/arcextent; 
	}

	//midx += Math.floor(Math.random()*50-25);
	//midy += Math.floor(Math.random()*50-25);
	
	return "M " + fromx + "," + fromy + " Q " + midx + "," + midy + " " + tox + "," + toy;
	
}

//function getMidPoint(from, to) {
//	var c1 = getCenter(from);
//	var c2 = getCenter(to);
//	
//	var dx = c2.x - c1.x,
//		dy = c2.y - c1.y,
//		dr = Math.sqrt(dx * dx + dy * dy);
//
//	var X = (c2.x + c1.x) / 2;
//    var Y = (c2.y + c1.y) / 2;
//    
//    var len = dr - ((dr/2) * Math.sqrt(3));
//    
//    X = X + (dy * len/dr);
//    Y = Y + (-dx * len/dr);
//    
//    return {s: c1, e: c2, m: {x : X, y : Y}, dr: dr};	
//}

function getCenter(name) {
	var node = d3.select("#"+name);
	
	if (node.empty()) {
		for (var i = 0; i < centers.length; i++)
			if (centers[i].node == name)
				var res = { x:parseFloat(centers[i].cx), y:parseFloat(centers[i].cy) };
	} else		 
		var res = { x:parseFloat(node.attr("cx")), y:parseFloat(node.attr("cy")) };
	
	return res;
}

function arcStroke(timeid) {
	
	var time = Math.floor((timeid-288/2) * 250/144);
	
	b = 100;
	if (time >= 0) {
		r = 250-time;
		g = 250-time;
	} else {
		r = 250+time;
		g = 250+time;
	}
	return "RGB("+r+","+g+","+b+")";
}

//function getNodeLabelX(d) {
//	var c = getCenter(d.type);
//	var x = c.x;
//	var r = NodeRay(d);
//	
//	return x + r + 10;
//}
//
//function getNodeLabelY(d) {
//	var c = getCenter(d.type);
//	var y = c.y;
//	
//	return y;
//}

var NodeRadius = function(d,max) {
	return (parseInt(d.percentage*100) * maximumNodeRadius / (Math.floor(100*max)))*increase_factor;
};

function findMaximumNodeSize(d) {
//	console.log(d);
	var max = d[0].percentage;
	for (var i=1; i<d.length; i++) {
		if (max < d[i].percentage) {
			max = d[i].percentage;
		}
	}
	return max;
}
//-----------------------------------------------------------------
//>>>TIME SLIDER>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------

//$('#timeslider').slider({
//		formatter: function(value) {
//			var hrs = Math.floor(value*5/60);
//			var mins = value*5 - hrs * 60;
//			return 'Time(' + hrs + ':' + mins + ')';
//		}
//	})
//	.data('slider');
//$('#timeslider').on('slideStop', timeChange)
////
//function timeChange() {
//	console.log("timeslider.getValue(): ", $('#timeslider').data('slider').getValue());
//	var timeInterval = $('#timeslider').data('slider').getValue();
//	redrawMapNodeLinkDiagram(timeInterval[0],timeInterval[1]);
//	var hrs1 = Math.floor(timeInterval[0]*5/60);
//	var mins1 = timeInterval[0]*5 - hrs1 * 60;
//	var hrs2 = Math.floor(timeInterval[1]*5/60);
//	var mins2 = timeInterval[1]*5 - hrs2 * 60;
//	
//	//$('#timesliderlabel').text(hrs1 + ":" + mins1 + "-" + hrs2 + ":" + mins2);
//};


    
//var timeChange = function() {
//	console.log("timeslider.getValue(): ", timeslider.getValue());
//	timeInterval = timeslider.getValue();
//	redrawMapNodeLinkDiagram(timeInterval[0],timeInterval[1]);
//	var hrs1 = Math.floor(timeInterval[0]*5/60);
//	var mins1 = timeInterval[0]*5 - hrs1 * 60;
//	var hrs2 = Math.floor(timeInterval[1]*5/60);
//	var mins2 = timeInterval[1]*5 - hrs2 * 60;
//	
//	//$('#timesliderlabel').text(hrs1 + ":" + mins1 + "-" + hrs2 + ":" + mins2);
//};
//
//var timeslider = $('#timeslider').slider({
//		formatter: function(value) {
//			var hrs = Math.floor(value*5/60);
//			var mins = value*5 - hrs * 60;
//			return 'Time(' + hrs + ':' + mins + ')';
//		}
//	})
//	.on('slideStop', timeChange)
//    .data('slider');


//-----------------------------------------------------------------
//>>>DRAG EVENTS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------
function nodelinkdragstart() {
//	console.log("dragstart");
}

function nodelinkdragmove() {
//	console.log("drag");
	
}

function nodelinkdragend() {
//	console.log("dragend");
	
}
//-----------------------------------------------------------------
//>>>EVENTS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//-----------------------------------------------------------------

function onArcOver(node, d, arc) {
	// Highlight arc
	var g = d3.select("#mapNodeLinkDiagram");
	
	//hrs min calculation
	var format = d3.time.format("%H:%M");
	var hrs = Math.floor(d.id*5/60);
	var mins = d.id*5 - hrs * 60;
	var time = new Date(2000,1,1,hrs,mins,0,0);
	
	// Append label
	var xy = d3.mouse(node);
	var g2 = g.append("g")
	          .attr("class","arclabelbox");
	
	g2.append("rect")
	  .attr("x",xy[0]+10)
	  .attr("y",xy[1]-10)
	  .attr("width",40)
	  .attr("height",20)
	  .attr("fill","#fff")
	  .attr("opacity",0.7);
	  
	g2.append("text")
		.attr("class", "arclabel")
		.attr("x", xy[0] + 10)
		.attr("y", xy[1]+5)
		.text(format(time));
	
	//show on timeline the timeline indicator
	var timelinesvg = d3.select("#timeline");
	timelinesvg.append("line")
			   .attr("class","timelineIndicator")
			   .attr("x1",d.id*timelineStrech + timelineDiagramMarginLeft)
			   .attr("y1",0)
			   .attr("x2",d.id*timelineStrech + timelineDiagramMarginLeft)
			   .attr("y2",timelineDiagramHeight - timelineDiagramMarginBottom)
			   .attr("stroke","RGB(100,100,100)")
			   //.attr("stroke-width","0.5")
			   .attr("stroke-dasharray","5,5");
}

function onArcOut(node) {
	var g = d3.select(node);
	d3.select(".arclabel").remove();
	d3.select(".arclabelbox").remove();
	
	//remove the timeline indicator
	var timelinesvg = d3.select("#timeline");
	timelinesvg.select(".timelineIndicator").remove();
}
//-------------------------------------------------------------

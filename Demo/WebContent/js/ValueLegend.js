/**
 * 
 */

function ValueLegend(rtm, primary) {
	
	this.config = rtm.config;
	var self = this;
	
	// "init"
	if (primary) {
		var table = $("<table></table>")
			.attr("class", "table table-hover");
		table.append(
			$("<thead></thead>").append(
				$("<tr></tr>")
					.append($("<th></th>"))
					.append($("<th>Value</th>"))));
		table.append(
			$("<tbody></tbody>")
				.attr("id", "valuelegendbody"));
		
		$("#valuelegend").append(table);
	} else {
		$("#valuelegendbody").append($("<br>"));
	}
	// -------------
	
	// add rows
	var colorIdx = 0;
	for(var i = self.config.value.visualize[0]; i <= self.config.value.visualize[1]; i++) {
		var tr = $("<tr></tr>")
		tr.append(			// colored circle
			$("<td></td>").append(
				$("<div></div>")
					.attr("id", "legend"+ self.config.value.prefix + PARAM.indexdd(i))
					.attr("class", "selection_marker tdcenter")
					.css("background-color", self.config.value.color[colorIdx])
					.css("cursor", "pointer")));
		tr.append(			// value name
			$("<td></td>").append(
				$("<div></div>")
					.html(self.config.value.label[colorIdx])));
		$("#valuelegendbody").append(tr);
				
		// value's color in rgb
		var rgba = self.config.value.color[colorIdx].match(/(\d{1,})/g);
		$("#legend" + self.config.value.prefix + PARAM.indexdd(i)).colorpicker({
			format: "rgb",
			color: d3.rgb(rgba[0], rgba[1], rgba[2]).toString()
		});
		$("#legend" + self.config.value.prefix + PARAM.indexdd(i)).colorpicker().on('changeColor', function(ev) { 
			var rgb = ev.color.toRGB();
			var color = "RGBA(" + rgb.r + "," + rgb.g + "," + rgb.b + ",1)";
			
			if ($.inArray(color, self.config.value.color) === -1) {
				var unit = d3.scale.linear()
					.domain([self.config.value.visualize[0], self.config.value.visualize[1]])
					.range([0, self.config.value.visualize[1]-self.config.value.visualize[0]]);
				var index = parseInt($(ev.currentTarget).attr("id").substring(("legend" + self.config.value.prefix).length));
				var layer = d3.selectAll("#"+general_config.radialTree.id + " defs stop").filter(function() {
					return $(this).attr("stop-color") === self.config.value.color[unit(index)];
				});
				
				self.config.value.color[unit(index)] = color;
				
				d3.select(ev.currentTarget).style("background-color", color);
				layer.attr("stop-color", color);
			}
			
			Log().add("colorChange", [$(this).attr("id").substring("legend".length), color]);
		});
		
		colorIdx++;
	}
	// ---------------------
}
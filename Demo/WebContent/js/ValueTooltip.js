/**
 * 
 */

// Constructor
function ValueTooltip(graphs) {

	if (!(this instanceof ValueTooltip)) {
		return new ValueTooltip(graphs);
	}
	// Singleton - part I
	var self = this;
	
	// init()
	this.values = graphs;
	// -----
	
	this.show = function(d, top, left) {
//		var title = (d.date) ? moment(d.date, general_config.date.formatIn).format(general_config.date.formatOut) : d.name;
		var text = "<strong>" + d.name + "</strong>";
		var v = d.aggregate || d;
		
		for(var i = 0; i < self.values.length; i++) {
			var label = 0;
			for(var j = self.values[i].config.value.visualize[0]; j <= self.values[i].config.value.visualize[1]; j++) {
				var value = self.values[i].config.value.type === 'percentage' ? PARAM.percentage(v[self.values[i].config.value.prefix + PARAM.indexdd(j)]) : d3.round(v[self.values[i].config.value.prefix + PARAM.indexdd(j)], 2);
				text += "<p>" + self.values[i].config.value.label[label] + ":   " + value + "</p>";
				label ++;
			}
			text += "<br>";
		}
		if (d.depth === general_config.dataset.depth)
			text += '<p style="align:right;">Press <em>SHIFT</em> for details</p>';
		
		var offsetTop = 8
			offsetLeft = 8;
		var pTop = top + offsetTop + "px"
			pLeft = left + offsetLeft + "px";
			
		self.remove();
		var tooltip = $("<div></div>")
			.attr("class", "ddtooltip")
			.css("top", pTop)
			.css("left", pLeft)
			.html(text);
	
		$("body").append(tooltip);
	};
	
	this.remove = function() {
		d3.select(".ddtooltip").remove();
	};

	// Singleton - part II
	ValueTooltip = function() {
		return self;
	};
}
/**
 * 
 */
// SINGLETON
function SelectionLegendPanel() {
	
	// Singleton - part I
	var self = this;

	this.BODY_ID = "legendBody";
	this.ROW_ID = "legendRow";
	this.DP_PREFIX = "datepicker";
	this.DP_BEGIN = this.DP_PREFIX+"Begin";
	this.DP_END = this.DP_PREFIX+"End";
	
	// Initialize the SelectionLegendPanel table.
	var table = $("<table></table>")
			.attr("class", "table table-hover");
			
	var thead = $("<tr></tr>");
	thead.append("<th></th>");
	thead.append("<th>Start</th>");
	thead.append("<th>End</th>");
	thead.append("<th></th>");
	
	table.append(
		$("<thead></thead>").append(thead));
	
	table.append(
		$("<tbody></tbody>")
			.attr("id", self.BODY_ID));
	
	table.append(
		$("<tr></tr>")
			.append($("<td></td>"))
			.append($("<td></td>"))
			.append($("<td></td>"))
			.append(
				$("<td></td>")
					.append(
						$("<div></div>")
							.append(
								$("<button></button>")
									.attr("type", "button")
									.attr("class", "close tdcenter")
									.on("click", function() { 
										Log().add("removeAllSelection");
										return Selection.removeAll();
									})
									.html("<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>")
									.attr("title","Clear all selections!")))));
	
	$("#legendpanel").append(table);
	// -------------------------------
	
	// Add a row relative to a new selection to the SelectionLegendPanel.
	this.addRow = function(selection) {
		var index = String(selection.index).replace('.','_');		// Needed by the selectors ($ and d3).
		// Table's row
		var tr = $("<tr></tr>")
			.attr("id", self.ROW_ID+index);
		
		tr.append(			// colored circle
			$("<td></td>").append(
				$("<div></div>")
					.attr("class", "selection_marker tdcenter")
					.css("background-color", general_config.selection.color[selection.color-1])
					.css("opacity", function() { return (selection.status === 'visible') ? null :  general_config.selection.opaque; })));
		
		tr.append(			// start date
			$("<td></td>").append(
				$("<div></div>")
					.append(
						$("<input/>")
							.attr("type", "text")
							.attr("id", self.DP_BEGIN+index)
							.attr("class", "form-control tdcenter"))));
		
		tr.append(			// end date
			$("<td></td>").append(
				$("<div></div>")
					.append(
						$("<input/>")
							.attr("type", "text")
							.attr("id", self.DP_END+index)
							.attr("class", "form-control tdcenter"))));
		
		if ((selection.index % 1) === 0) {
			tr.append(			// remove button
				$("<td></td>").append(
					$("<div></div>").append(
						$("<button></button>")
							.attr("type", "button")
							.attr("class", "close tdcenter")
							.on("click", function() {
								Log().add("removeSelection", selection);
								return Selection.remove(selection.index);
							})
							.html("<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>"))));	// bootstrap style
		}
		
		$("#"+self.BODY_ID).append(tr);
		// -----------------
		
		// ------------- datepicker elements - bootstrap -----------------------------
		var startDate = moment(d3.select("[id*='arc"+selection.start+"']").datum().name, general_config.date.formatIn);
		var endDate = moment(d3.select("[id*='arc"+selection.end+"']").datum().name, general_config.date.formatIn);

		var first = "$", last = "$";
		for(var i = 0; i < general_config.dataset.depth; i++) {
			first += ".children[0]"; last += ".children[-1:]";
		}
			
		var firstDate = moment(jsonPath(data_structure, first)[0].name, general_config.date.formatIn);
		var lastDate = moment(jsonPath(data_structure, last)[0].name, general_config.date.formatIn);
		
		// common options
		$.fn.datepicker.defaults.format = "dd/mm/yy";
		$.fn.datepicker.defaults.autoclose = "true";
		
		// start date
		$('#'+self.DP_BEGIN+index).datepicker({ 
			endDate: endDate.toDate(),
			startDate: firstDate.toDate()
		});
		$('#'+self.DP_BEGIN+index).datepicker("setDate", startDate.toDate());

		// end date		
		$('#'+self.DP_END+index).datepicker({ 
			startDate: startDate.toDate(),
			endDate: lastDate.toDate()
		});
		$('#'+self.DP_END+index).datepicker("setDate", endDate.toDate());
		
		if ((selection.index % 1) === 0) {
			$('#'+self.DP_BEGIN+index).datepicker()		// onStartDateChange 
				.on("changeDate", function(e) {
					$('#'+self.DP_END+index).datepicker("setStartDate", e.date);
					Selection.edit(selection.index, e.date, "start");
				});
			$('#'+self.DP_END+index).datepicker()			// onEndDateChange 
				.on("changeDate", function(e) {
					$('#'+self.DP_BEGIN+index).datepicker("setEndDate", e.date);
					Selection.edit(selection.index, e.date, "end");
				});
		} else {
			$('#'+self.DP_BEGIN+index).attr("disabled", true);		// disable (only for sub-selections).
			$('#'+self.DP_END+index).attr("disabled", true);
		}
		// -----------------------------------------------------------------------------
	};
	
	// Delete the row relative to the selection[index] from the SelectionLegendPanel table
	// and removes the two associated datepicker elements.
	this.removeRow = function(idx) {
		var index = String(idx).replace('.', '_');
		$("#"+self.ROW_ID+index).remove();
		$('#'+self.DP_BEGIN+index).datepicker('destroy');
		$('#'+self.DP_END+index).datepicker('destroy');
	};
	
	// Delete the whole SelectionLegendPanel table and all the datepicker elements.
	this.clear = function() {
		$("[id^='"+self.ROW_ID+"']").remove();
		$("[id^='"+self.DP_PREFIX+"']").remove();
	};
	
	// Singleton - part II
	SelectionLegendPanel = function() {
		return self;
	};
	
}

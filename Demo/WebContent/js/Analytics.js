/**
 * 
 */

function Analytics(selected) {
	var all = "$";
	for (var i = 1; i < general_config.dataset.depth; i++)
		all += ".children[*]";
	
	var condition = "";
	for (var j = 0; j < instances.length; j++) {
		var cndt = "";
		var thr = d3.round($('#' + instances[j].config.RadialTree.prefix + 'slider').data("slider").getValue(), 2)
		for(var i = instances[j].config.value.visualize[0]; i <= instances[j].config.value.visualize[1]; i++) {
			var v = instances[j].config.value.prefix + PARAM.indexdd(i);
			cndt += "@." + v + ">=" + selected[v]*(1-thr) + " && @." + v + "<=" + selected[v]*(1+thr) + " && ";
		}
		condition += cndt;
	}
	condition = condition.substring(0, condition.length-4);
	
	var matches = jsonPath(data_structure, all + ".children[?(" + condition + ")]");
	
	var result = {};
	var index = $.inArray(selected, matches);
	result.referenceDay = matches.splice(index, 1)[0];
	result.similarDays  = matches;
	
	return result;
}
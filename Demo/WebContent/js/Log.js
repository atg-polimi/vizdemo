/**
 * 
 */

function Log() {
	
	if (!(this instanceof Log)) {
		return new Log();
	}
	
	// Singleton I
	var instance = this;
	
	this.fileName = moment().format("YYYY-MM-DD-HH:mm:ss:SSS");
	this.content = "";
	this.counter = 0;
	this.index = 0;
	this.sendToLogger = function() {
		$.ajax({
			url: "http://alpine-carrier-727.appspot.com/log",
			type: "POST",
			crossDomain: true,
			data: { name: Log().fileName, content: Log().content, index: this.index++ },
			success: function(data, textStatus, jqXHR) {
//				console.log("sent" + data + " .. " + textStatus + " -- " + jqXHR);
			},
			error: function(data, status, error) {
				console.log("error: "+ data + " status: " + status + " er:" + error);
			}
		})
	};
	
	Log.prototype.add = Log.add;
	
	// Singleton II
	Log = function() {
		return instance;
	};
}

Log.add = function(event, param, viewm) {
	var view = "";
	var action = "";
	switch (event) {
		case "mouse":
			view = viewm;
			action = "mouse, " + param[0] + ", " + param[1];
			break;
		case "colorChange":
			view = "overview";
			action = "colorChange, " + param[0] + ", " + param[1];
			break;
		case "mouseover":
			view = "overview";
			action = "mouseover, " + PARAM.mouse.x + ", " + PARAM.mouse.y + ", " + param.name;
			break;
		case "mouseout":
			view = "overview";
			action = "mouseout, " + PARAM.mouse.x + ", " + PARAM.mouse.y + ", " + param.name;
			break;
		case "selection":
			view = "overview";
			var start = moment(d3.select("[id*='arc"+param[0]+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			var end = moment(d3.select("[id*='arc"+param[1]+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			action = "selection, " + start + ", " + end;
			break;
		case "zoom":
			view = "overview";
			action = "zoom, " + param.name;
			break;
		case "displaySimilarity":
			view = "overview";
			action = "displaySimilarity";
			break;
		case "displaySelection":
			view = "overview";
			action = "displaySelection"
			break;
		case "rangePanelIn":
			view = "overview";
			action = "rangePanelIn"
			break;
		case "rangePanelOut":
			view = "overview";
			action = "rangePanelOut"
			break;
		case "histogramPanelIn":
			view = "overview";
			action = "histogramPanelIn"
			break;
		case "histogramPanelOut":
			view = "overview";
			action = "histogramPanelOut"
			break;
		case "removeSelection":
			view = "overview";
			var start = moment(d3.select("[id*='arc"+param.start+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			var end = moment(d3.select("[id*='arc"+param.end+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			action = "remeoveSelection, " + start + ", " + end;
			break;
		case "removeAllSelection":
			view = "overview";
			action = "removeAllSelection";
			break;
		case "editSelection":
			view = "overview";
			var start = moment(d3.select("[id*='arc"+param.start+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			var end = moment(d3.select("[id*='arc"+param.end+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			action = "editSelection, " + start + ", " + end;
			break;
		case "unlockedDate":
			view = "overview";
			action = "unlockedDate";
			break;
		case "setRefDay":
			view = "overview";
			var day = moment(d3.select("[id*='arc"+param.id+"_']").datum().name, general_config.date.formatIn).format(general_config.date.formatIn);
			action = "setRefDay, " + day;
			break;
		case "thrChanged":
			view = "overview";
			action = "thrChanged, " + param[0] + ", " + param[1];
			break;
		case "setRefDayCalendar":
			view = "overview";
			action = "setRefDayCalendar, " + moment(param).format(general_config.date.formatIn);
			break;
		case "mouseOnSimilar":
			view = "overview";
			action = "mouseOnSimilar, " + param[0] + ", " + param[1];
			break;
		case "nodeLinkIn":
			view = "detailed";
			action = "nodeLinkIn";
			break;
		case "nodeLinkOut":
			view = "detailed";
			action = "nodeLinkOut";
			break;
		case "timeLineIn":
			view = "detailed";
			action = "timeLineIn";
			break;
		case "timeLineOut":
			view = "detailed";
			action = "timeLineOut";
			break;
		case "onNode":
			view = "detailed";
			action = "onNode, " + param.type;
			break;
		case "outNode":
			view = "detailed";
			action = "outNode, " + param.type;
			break;
		case "onLink":
			view = "detailed";
			action = "onLink, " + param.value + ", " + param.id;
			break;
		case "outLink":
			view = "detailed";
			action = "outLink, " + param.value + ", " + param.id;
			break;
		case "timeLineSlider":
			view = "detailed";
			action = "timeLineSlider, " + param;
			break;
		case "timeLineSelection":
			view = "detailed";
			action = "timeLineSelection, " + param[0] + ", " + param[1];
			break;
		case "onSegment":
			view = "detailed";
			action = "onSegment, " + param.value + ", " + param.startId + ", " + param.endId;
			break;
		case "outSegment":
			view = "detailed";
			action = "outSegment, " + param.value + ", " + param.startId + ", " + param.endId;
			break;
		case "showMicro":
			view = "overview";
			action = "showMicro, " + param;			
			break;
		case "showMacro":
			view = "detailed";
			action = "showMacro";
			break;
	};
	
//	Log().content += moment().format("YYYY-MM-DD-HH:mm:ss:SSS") + ", " + view + ", " + action + "\r\n";
	Log().content += moment() + ", " + view + ", " + action + "\r\n";
	Log().counter++;
	
	if (Log().counter === 200) {
		Log().sendToLogger();
		Log().content = "";
		Log().counter = 0;
	}
};
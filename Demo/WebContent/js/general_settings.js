/**
 * 
 */
var general_config = 
{
	"dataset": {
		"depth": 3
	},
	"radialTree": {
		"id": "radialtree",
		RadialBound: 1.995 * Math.PI,
		"maxRadius": 0,
		"offset": 30
	},
	"selection": {
		"MAX_INDEX": 5,
		"label": [1, 2, 3, 4, 5],
		"color": ["RGBA(255,0,0,1)", "RGBA(9,112,84,1)", "RGBA(0,0,255,1)", "RGBA(255,255,0,1)", "RGBA(128,0,128,1)"],
		"labelColor": ["RGBA(255,0,0,0.65)", "RGBA(9,112,84,0.65)", "RGBA(0,0,255,0.65)", "RGBA(255,255,0,0.65)", "RGBA(128,0,128,0.65)"],
		"opaque": 0.4,
		"markerOffset": [0, 6, 12, 18, 24]
	},
	"similarity": {
		"stokeOn": "RGB(237,0,244)",//"RGB(0,128,0)",
		"fill": "RGB(237,0,244)",//"RGB(0,128,0)",
		"fillSel": "RGB(0,255,0)"
	},
	"date": {
		"formatIn": "DD/MM/YYYY",
		"formatOut": "DD/MM/YYYY",
		"sliderFormat": "dd/mm/yy",
		"popoverFormat": "DD MMM YYYY"
	}
}
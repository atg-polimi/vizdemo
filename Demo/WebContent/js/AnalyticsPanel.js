/**
 * 
 */

function AnalyticsPanel(rtm, primary) {
	
	this.rtm = rtm;
	
	var self = this;
	
	// Initialization	
	$("#similarityMode").append(
			$("<div></div>")
				.attr("class", "analyticspanel col-md-7 col-md-offset-3")
				.css("visibility", "visible"));
	
	var APheader = $("<div></div")
		.attr("class", "APheader");
	
	$(".analyticspanel").append(APheader);
	if (primary) {
		APheader.append(
			$("<h4></h4>")
				.attr("class", "small")
				.html("Reference day: "))
			.append(
				$("<div></div>")
					.attr("id", "refdaySection")
					.append(
					$("<h4></h4>")
						.attr("id", "refday")
						.css("cursor", "pointer")
						.html("------")))
			.append(
				$("<img>")
					.attr("class", "imgcenter")
					.attr("src", "icon/glyphicons-205-unlock.png")
					.attr("height", "16px"));
	}
		
	APheader.append(
			$("<h4></h4>")
				.attr("class", "small")
				.html("Threshold"))
			.append(	// Slider
				$("<div></div>")
					.attr("id", self.rtm.config.RadialTree.prefix + "slider"))
	
	var APbody = $("<div></div>")
		.attr("class", "APbody");
	$(".analyticspanel").append(APbody);
	
//	for (var i = 0; i < self.rtm.config.value.length; i++) {
		var counter = 0;
		for(var j = self.rtm.config.value.visualize[0]; j <= self.rtm.config.value.visualize[1]; j++) {
			APbody.append(
				$("<div></div>")
					.attr("id", "AP"+self.rtm.config.value.prefix+PARAM.indexdd(j))
//					.css("margin-top", "30px")
					.append(
						$("<h5></h5>")
							.html(self.rtm.config.value.label[counter]))
					.append(
						$("<div></div>")
							.attr("class", self.rtm.config.value.prefix+PARAM.indexdd(j)+"line")));
			
			var svg = d3.select("."+self.rtm.config.value.prefix+PARAM.indexdd(j)+"line")
				.append("svg")
					.style("height", "50px")
				.append("g");
			svg.append("line")	// main line
				.attr("x1", "10%")
				.attr("x2", "90%")
				.attr("y1", "40px")
				.attr("y2", "40px");
			svg.append("line")		// middle value
				.attr("x1", "50%")
				.attr("x2", "50%")
				.attr("y1", "30px")
				.attr("y2", "50px");
			svg.append("line")		// min value
				.attr("x1", "10%")
				.attr("x2", "10%")
				.attr("y1", "36px")
				.attr("y2", "44px");
			svg.append("line")		// max value
				.attr("x1", "90%")
				.attr("x2", "90%")
				.attr("y1", "36px")
				.attr("y2", "44px");
			counter++;
		}
		
//	}
	$(".APbody").css("visibility", "hidden");
	// ------------------
	
	// Slider initialization - seiyria
	var format0 = d3.format(".0%");
	$("#"+ self.rtm.config.RadialTree.prefix + "slider").slider({
		id: "slider0",
		min: 0,
		max: 1,
		step: 0.05,
		value: 0.1,
		tooltip: 'always',
		formatter: function(value) {
			return format0(value);
		}
	});
	// -------------------------------
	
	if (primary) {
		// Date picker definition
		$.fn.datepicker.defaults.format = general_config.date.sliderFormat;//APP.date.sliderFormat;
		$.fn.datepicker.defaults.autoclose = "true";
	
		$("#refday").on("click", function() {
			$("#refday").css("display", "none");
			
			$("#refdaySection").append(
					$("<input type='text' />")
						.attr("id", "refdaypicker")
						.attr("class", "form-control"));
			
			var first = "$", last = "$", all = "$";
			for(var i = 0; i < general_config.dataset.depth; i++) {
				first += ".children[0]"; last += ".children[-1:]"; 
			}
			for (var i = 1; i < general_config.dataset.depth; i++)
				all += ".children[*]";
			
			$('#refdaypicker').datepicker({
				startDate: moment(jsonPath(data_structure, first)[0].name, general_config.date.formatIn).toDate(),
				endDate: moment(jsonPath(data_structure, last)[0].name, general_config.date.formatIn).toDate()
			});
			
			var refDate = (AnalyticsPanel.referenceDay == undefined) ? jsonPath(data_structure, first)[0] : AnalyticsPanel.referenceDay;
			$('#refdaypicker').datepicker('setDate', moment(refDate.name, general_config.date.formatIn).toDate());
			
			$('#refdaypicker').datepicker()
				.on("changeDate", function(e) {
					$("#refday").html(moment(e.date).format(general_config.date.formatOut));
					
					dehighlightDays({referenceDay: AnalyticsPanel.referenceDay, similarDays: AnalyticsPanel.similarDays});
//					
					var newReferenceDay =
						jsonPath(data_structure, all+".children[?(@.name == " + moment(e.date).format(general_config.date.formatIn) + ")]")[0]
						|| d3.selectAll("[id*='arc']").filter(function(d) { 
							return d.name === moment(e.date).format(general_config.date.formatIn);
						}).datum();//data()[0];
					
					var matches = new Analytics(newReferenceDay);
					self.draw(matches, true);
					highlightDays(matches);
					
					$(this).datepicker('remove');
					$("#refdaypicker").remove();
					$("#refday").css("display", "inherit");
					
					Log().add("setRefDayCalendar", e.date);
				});
			$('#refdaypicker').datepicker('show');
			
		})
	}
	// -------------------------------
	
	this.draw = function(matches, clicked) {
		AnalyticsPanel.referenceDay = matches.referenceDay;
		AnalyticsPanel.similarDays = matches.similarDays;
		
		$(".APbody").css("visibility", "visible");
		
		if (clicked) {
			AnalyticsPanel.locked = true;
			$(".imgcenter")
				.attr("src", "icon/glyphicons-204-lock.png")
				.css("cursor", "pointer")
				.on("click", function() {
					AnalyticsPanel.locked = false;
					dehighlightDays({referenceDay: AnalyticsPanel.referenceDay, similarDays: AnalyticsPanel.similarDays});
					$(".imgcenter")
						.attr("src", "icon/glyphicons-205-unlock.png")
						.css("cursor", "auto");
					$("#refday").html("------");
					
					Log().add("unlockedDate")
				});
		}
			$("#refday").html(moment(AnalyticsPanel.referenceDay.name, general_config.date.formatIn).format(general_config.date.formatOut) + "  :  " + AnalyticsPanel.similarDays.length  + " matches");
			
			var thr = d3.round($("#" + rtm.config.RadialTree.prefix + "slider").data('slider').getValue(), 2)
			
			for(var i = 0; i < instances.length; i++) {
				var counter = 0;
				for(var j = instances[i].config.value.visualize[0]; j <= instances[i].config.value.visualize[1]; j++) {
					var svg = d3.select("."+instances[i].config.value.prefix+PARAM.indexdd(j)+"line").select("svg");
					svg.select(".visualinfo")
						.remove();
					svg = svg.append("g")
							.attr("class", "visualinfo");
						
					var value = AnalyticsPanel.referenceDay[instances[i].config.value.prefix+PARAM.indexdd(j)];
					svg.append("text")
						.attr("id", "middletext")
						.attr("x", "45%")
						.attr("y", "10px")
						.text(function(d) {
							var text = (instances[i].config.value.type === 'percentage') ? PARAM.percentage(value) : d3.round(value, 2);
					    	return text;
						})
						.attr("text-decoration","underline");
					
					var min = value * (1 - thr);
					svg.append("text")
						.attr("x", "6%")
						.attr("y", "12px")
						.text(function(d) {
							var text = (instances[i].config.value.type === 'percentage') ? PARAM.percentage(min) : d3.round(min, 2);
					    	return text;
						});
					
					var max = value * (1 + thr);
					svg.append("text")
						.attr("x", "86%")
						.attr("y", "12px")
						.text(function(d) {
							var text = (instances[i].config.value.type === 'percentage') ? PARAM.percentage(max) : d3.round(max, 2);
					    	return text;
						});

					
					// Similar days points
					var s = d3.scale.linear()
						.domain([min, value, max])
						.range([10, 50, 90]);
					printSimilarDays(svg, instances[i].config.value.prefix+PARAM.indexdd(j), AnalyticsPanel.similarDays, s);
				}
				counter++;
			}
			
			$("#" + rtm.config.RadialTree.prefix + "slider").slider()
				.on('slideStop', function(evt) {
					if (evt.timeStamp != -1) {
						if (AnalyticsPanel.locked) {
							dehighlightSimilar(AnalyticsPanel.similarDays);
							var matches = new Analytics(AnalyticsPanel.referenceDay);
							self.draw(matches, false);
							highlightDays(matches);
						}
						evt.timeStamp = -1;
						
						Log().add("thrChanged", [rtm.config.RadialTree.prefix, d3.round($(this).data("slider").getValue(), 2)]);
					}
				});
	};
	
	var printSimilarDays = function(box, value, similardays, s) {
		var	circle = box.selectAll("circle").data(similardays, function(d) {return d.name;});
		circle.enter()
			.append("circle")
				.attr("class", "point"+value)
				.attr("cx", function(d) { return s(d[value]) + "%"; })
				.attr("cy", "40px")
				.attr("r", "4px")
				.attr("fill", general_config.similarity.fill)
				.on("mouseover", function(d) {
					d3.selectAll("circle").filter(function(f) {		// pointed day
						return d.name === f.name;
					}).attr("fill", "green")
					  .attr("r", "6px")
					  .style("cursor", "pointer");
					
					var dates = d3.selectAll(".point"+value).filter(function(f) {
						return d[value] === f[value];
					}).data();
					
					// popover
					$(this).popover({
						container: 'body',
						trigger: 'click',
						placement: 'right',
						html: 'true'
					});
					
					var html = "<div class='text-center'><strong>" + moment(d.name, general_config.date.formatIn).format(general_config.date.popoverFormat) + "</strong><br></div>";
					if (dates.length > 1) {
						html += "<div class='text-center'><strong>" + (dates.length-1) + "</strong>" + " day(s) with the same value:</div>"
						html += "<table>";
						for(var e in dates) {
							if (dates[e].name != d.name) {
								var text = "<td class='col-md-1'>" + moment(dates[e].name, general_config.date.formatIn).format(general_config.date.popoverFormat) + "</td>";
								if ((e % 2) == 0)
									html += "<tr>" + text;
								else
									html += text + "</tr>";
							}
						}
						html += "</table>";
					}
					$(this).data('bs.popover').options.content = html;
					// ----------------------
					
					for(var i = 0; i < instances.length; i++) {
						for(var j = instances[i].config.value.visualize[0]; j<= instances[i].config.value.visualize[1]; j++) {		// similar days with the same room-value 
							var s = d3.selectAll(".point"+instances[i].config.value.prefix+PARAM.indexdd(j)).filter(function(f) {
								return d[instances[i].config.value.prefix+PARAM.indexdd(j)] === f[instances[i].config.value.prefix+PARAM.indexdd(j)];
							});
							s.attr("fill", "green")
							  .attr("r", "6px");
	
							var node = s[0][0];
							var textX = parseFloat($(node).attr("cx")) - 5.0;
							var textY = parseFloat($(node).attr("cy")) - 13.0;
							var field = $(node).attr("class").substring("point".length);
							d3.select(node.parentNode).append("text")	// pointed day's caption
								.attr("class", "caption")
								.attr("x", textX+"%")
								.attr("y", textY+"px")
								.attr("fill", "green")
								.text(function() {
									var text = (instances[i].config.value.type === 'percentage') ? PARAM.percentage(d[field]) : d[field];
							    	return text;
								});
						}
					}
										
					var sd = d3.selectAll("path[id*='arc']").filter(function(f) {		// arc related to the pointed day
						return d.name === f.name;
					}).attr("stroke", "green")
					  .style("fill", general_config.similarity.fillSel);
					
					Log().add("mouseOnSimilar", [value, d3.select(sd[0][0]).datum().name]);
				})
				.on("mouseout", function(d) {
					d3.selectAll("circle").filter(function(f) {
						return d.name === f.name;
					}).attr("fill", general_config.similarity.fill)
					  .attr("r", "4px");
					
					for(var i = 0; i < instances.length; i++) {
						for(var j = instances[i].config.value.visualize[0]; j<= instances[i].config.value.visualize[1]; j++) {
							d3.selectAll(".point"+instances[i].config.value.prefix+PARAM.indexdd(j)).filter(function(f) {
								return d[instances[i].config.value.prefix+PARAM.indexdd(j)] === f[instances[i].config.value.prefix+PARAM.indexdd(j)];
							}).attr("fill", general_config.similarity.fill)
							  .attr("r", "4px")
							  .style("cursor", "auto");
						}
						
						d3.selectAll("path[id*='arc']").filter(function(f) {
							return d.name === f.name;
						}).style("fill", function(d) {
							var t = d3.select(this).attr("id").split("arc");
							return "url(\#" + t[0] + "radialGradient"+ t[1].substring(0,t[1].length-1) +")"; 
						});
					}
					
					// popover out
					$(this).popover('destroy');
					// ------
					d3.selectAll(".caption").remove();
					
					d3.selectAll("[id*='arc']").filter(function(f) {
						return d.name === f.name;
					}).attr("stroke", general_config.similarity.stokeOn)
					  .attr("stroke-width", "1");
				})
		circle.exit()
			.remove();
	};
	
}

AnalyticsPanel.locked = false;
AnalyticsPanel.referenceDay = 0;
AnalyticsPanel.similarDays = 0;
//	var self = this;
//	
//	this.values = graphs;
//	
//	AnalyticsPanel.prototype.locked = false;
//	AnalyticsPanel.prototype.referenceDay = 0;
//	AnalyticsPanel.prototype.similarDays = 0;
//	
//	// Initialization	
//	$("#similarityMode").append(
//			$("<div></div>")
//				.attr("class", "analyticspanel col-md-7 col-md-offset-3")
//				.css("visibility", "visible"));
//	
//	var APheader = $("<div></div")
//		.attr("class", "APheader");
//	
//	$(".analyticspanel").append(APheader);
//	APheader.append(	// panel's title
//			$("<h4></h4>")
//				.attr("class", "small")
//				.html("Threshold"))
//		.append(	// Slider
//			$("<div></div>")
//				.attr("id", "slider"));
//	APheader.append(
//			$("<h4></h4>")
//				.attr("class", "small")
//					.html("Reference day: "))
//			.append(
//				$("<div></div>")
//					.attr("id", "refdaySection")
//					.append(
//						$("<h4></h4>")
//							.attr("id", "refday")
//							.css("cursor", "pointer")
//							.html("------")))
//			.append(
//				$("<img>")
//					.attr("class", "imgcenter")
//					.attr("src", "icon/glyphicons-205-unlock.png")
//					.attr("height", "16px"));
//	
//	var APbody = $("<div></div>")
//		.attr("class", "APbody");
//	$(".analyticspanel").append(APbody);
//	
//	for (var i = 0; i < self.values.length; i++) {
//		var counter = 0;
//		for(var j = self.values[i].config.value.visualize[0]; j <= self.values[i].config.value.visualize[1]; j++) {
//			APbody.append(
//				$("<div></div>")
//					.attr("id", "AP"+self.values[i].config.value.prefix+PARAM.indexdd(j))
//					.css("margin-top", "30px")
//					.append(
//						$("<h5></h5>")
//							.html(self.values[i].config.value.label[counter]))
//					.append(
//						$("<div></div>")
//							.attr("class", self.values[i].config.value.prefix+PARAM.indexdd(j)+"line")));
//			
//			var svg = d3.select("."+self.values[i].config.value.prefix+PARAM.indexdd(j)+"line")
//				.append("svg")
//					.style("height", "50px")
//				.append("g");
//			svg.append("line")	// main line
//				.attr("x1", "10%")
//				.attr("x2", "90%")
//				.attr("y1", "40px")
//				.attr("y2", "40px");
//			svg.append("line")		// middle value
//				.attr("x1", "50%")
//				.attr("x2", "50%")
//				.attr("y1", "30px")
//				.attr("y2", "50px");
//			svg.append("line")		// min value
//				.attr("x1", "10%")
//				.attr("x2", "10%")
//				.attr("y1", "36px")
//				.attr("y2", "44px");
//			svg.append("line")		// max value
//				.attr("x1", "90%")
//				.attr("x2", "90%")
//				.attr("y1", "36px")
//				.attr("y2", "44px");
//			counter++;
//		}
//		
//	}
//	$(".APbody").css("visibility", "hidden");
//	// ------------------
//	
//	// Slider initialization - seiyria
//	var format0 = d3.format(".0%");
//	$("#slider").slider({
//		id: "slider0",
//		min: 0,
//		max: 1,
//		step: 0.05,
//		value: 0.1,
//		tooltip: 'always',
//		formatter: function(value) {
//			return format0(value);
//		}
//	});
//	// -------------------------------
//	
//	// Date picker definition
//	$.fn.datepicker.defaults.format = general_config.date.sliderFormat;//APP.date.sliderFormat;
//	$.fn.datepicker.defaults.autoclose = "true";
//
//	$("#refday").on("click", function() {
//		$("#refday").css("display", "none");
//		
//		$("#refdaySection").append(
//				$("<input type='text' />")
//					.attr("id", "refdaypicker")
//					.attr("class", "form-control"));
//		
//		var first = "$", last = "$", all = "$";
//		for(var i = 0; i < general_config.dataset.depth; i++) {
//			first += ".children[0]"; last += ".children[-1:]"; 
//		}
//		for (var i = 1; i < general_config.dataset.depth; i++)
//			all += ".children[*]";
//		
//		$('#refdaypicker').datepicker({
//			startDate: moment(jsonPath(data_structure, first)[0].name, general_config.date.formatIn).toDate(),
//			endDate: moment(jsonPath(data_structure, last)[0].name, general_config.date.formatIn).toDate()
//		});
//		
//		var refDate = (AnalyticsPanel.referenceDay == undefined) ? jsonPath(data_structure, first)[0] : AnalyticsPanel.referenceDay;
//		$('#refdaypicker').datepicker('setDate', moment(refDate.name, general_config.date.formatIn).toDate());
//		
//		$('#refdaypicker').datepicker()
//			.on("changeDate", function(e) {
//				$("#refday").html(moment(e.date).format(general_config.date.formatOut));
//				
//				for(var i = 0; i < self.values.length; i++)
//					dehighlightDays({referenceDay: AnalyticsPanel.referenceDay, similarDays: AnalyticsPanel.similarDays}, self.values[i]);
//				
//				var newReferenceDay =
//					jsonPath(data_structure, all+".children[?(@.name == " + moment(e.date).format(general_config.date.formatIn) + ")]")[0]
////					jsonPath(rtm.data, "$.seasons[*].months[*].days[?(@.date == " + moment(e.date).format(APP.date.formatIn) + ")]")[0] 
//					|| d3.selectAll("[id*='arc']").filter(function(d) { 
//						return d.name === moment(e.date).format(general_config.date.formatIn);
//					}).datum();//data()[0];
//				
//				var matches = new Analytics(newReferenceDay);
//				AnalyticsPanel().draw(matches, true);
//				highlightDays(matches);
//				
//				$(this).datepicker('remove');
//				$("#refdaypicker").remove();
//				$("#refday").css("display", "inherit");
//				
//			});
//		$('#refdaypicker').datepicker('show');
//		
//	})
//	// -------------------------------
//	
//	this.draw = function(matches, clicked) {
//		AnalyticsPanel.referenceDay = matches.referenceDay;
//		AnalyticsPanel.similarDays = matches.similarDays;
//		
//		$(".APbody").css("visibility", "visible");
//		
//		if (clicked) {
//			AnalyticsPanel.locked = true;
//			$(".imgcenter")
//				.attr("src", "icon/glyphicons-204-lock.png")
//				.css("cursor", "pointer")
//				.on("click", function() {
//					AnalyticsPanel.locked = false;
//					for(var i = 0; i < self.values.length; i++)
//						dehighlightDays({referenceDay: AnalyticsPanel.referenceDay, similarDays: AnalyticsPanel.similarDays}, self.values[i]);
//					$(".imgcenter")
//						.attr("src", "icon/glyphicons-205-unlock.png")
//						.css("cursor", "auto");
//					$("#refday").html("------");
//				});
//		}
//			$("#refday").html(moment(AnalyticsPanel.referenceDay.name, general_config.date.formatIn).format(general_config.date.formatOut) + "  :  " + AnalyticsPanel.similarDays.length  + " matches");
//			
//			var thr = d3.round($("#slider").data('slider').getValue(), 2)
//			
//			for(var i = 0; i < self.values.length; i++) {
//				var counter = 0;
//				for(var j = self.values[i].config.value.visualize[0]; j <= self.values[i].config.value.visualize[1]; j++) {
//					var svg = d3.select("."+self.values[i].config.value.prefix+PARAM.indexdd(j)+"line").select("svg");
//					svg.select(".visualinfo")
//						.remove();
//					svg = svg.append("g")
//							.attr("class", "visualinfo");
//						
//					var value = AnalyticsPanel.referenceDay[self.values[i].config.value.prefix+PARAM.indexdd(j)];
//					svg.append("text")
//						.attr("id", "middletext")
//						.attr("x", "45%")
//						.attr("y", "10px")
//						.text(function(d) {
//							var text = (self.values[i].config.value.type === 'percentage') ? PARAM.percentage(value) : value;
//					    	return text;
//						})
//						.attr("text-decoration","underline");
//					
//					var min = value * (1 - thr);
//					svg.append("text")
//						.attr("x", "6%")
//						.attr("y", "12px")
//						.text(function(d) {
//							var text = (self.values[i].config.value.type === 'percentage') ? PARAM.percentage(min) : min;
//					    	return text;
//						});
//					
//					var max = value * (1 + thr);
//					svg.append("text")
//						.attr("x", "86%")
//						.attr("y", "12px")
//						.text(function(d) {
//							var text = (self.values[i].config.value.type === 'percentage') ? PARAM.percentage(max) : max;
//					    	return text;
//						});
//
//					
//					// Similar days points
//					var s = d3.scale.linear()
//						.domain([min, value, max])
//						.range([10, 50, 90]);
//					printSimilarDays(svg, self.values[i].config.value.prefix+PARAM.indexdd(j), AnalyticsPanel.similarDays, s);
//				}
//				counter++;
//			}
//			
//			$("#slider").slider()
//				.on('slideStop', function(evt) {
//					if (evt.timeStamp != -1) {
//						if (AnalyticsPanel.locked) {
//							dehighlightSimilar(AnalyticsPanel.similarDays);
//							var matches = new Analytics(AnalyticsPanel.referenceDay);
//							AnalyticsPanel().draw(matches, false);
//							highlightDays(matches);
//						}
//						evt.timeStamp = -1;
//					}
//			});
//	};
//	
//	var printSimilarDays = function(box, value, similardays, s) {
//		var	circle = box.selectAll("circle").data(similardays, function(d) {return d.name;});
//		circle.enter()
//			.append("circle")
//				.attr("class", "point"+value)
//				.attr("cx", function(d) { return s(d[value]) + "%"; })
//				.attr("cy", "40px")
//				.attr("r", "4px")
//				.attr("fill", general_config.similarity.fill)
//				.on("mouseover", function(d) {
//					d3.selectAll("circle").filter(function(f) {		// pointed day
//						return d.name === f.name;
//					}).attr("fill", "green")
//					  .attr("r", "6px")
//					  .style("cursor", "pointer");
//					
//					var dates = d3.selectAll(".point"+value).filter(function(f) {
//						return d[value] === f[value];
//					}).data();
//					
//					// popover
//					$(this).popover({
//						container: 'body',
//						trigger: 'click',
//						placement: 'right',
//						html: 'true'
//					});
//					
//					var html = "<div class='text-center'><strong>" + moment(d.name, general_config.date.formatIn).format(general_config.date.popoverFormat) + "</strong><br></div>";
//					if (dates.length > 1) {
//						html += "<div class='text-center'><strong>" + (dates.length-1) + "</strong>" + " day(s) with the same value:</div>"
//						html += "<table>";
//						for(var e in dates) {
//							if (dates[e].name != d.name) {
//								var text = "<td class='col-md-1'>" + moment(dates[e].name, general_config.date.formatIn).format(general_config.date.popoverFormat) + "</td>";
//								if ((e % 2) == 0)
//									html += "<tr>" + text;
//								else
//									html += text + "</tr>";
//							}
//						}
//						html += "</table>";
//					}
//					$(this).data('bs.popover').options.content = html;
//					// ----------------------
//					
//					for(var i = 0; i < self.values.length; i++) {
//						for(var j = self.values[i].config.value.visualize[0]; j<= self.values[i].config.value.visualize[1]; j++) {		// similar days with the same room-value 
//							var s = d3.selectAll(".point"+self.values[i].config.value.prefix+PARAM.indexdd(j)).filter(function(f) {
//								return d[self.values[i].config.value.prefix+PARAM.indexdd(j)] === f[self.values[i].config.value.prefix+PARAM.indexdd(j)];
//							});
//							s.attr("fill", "green")
//							  .attr("r", "6px");
//	
//							var node = s[0][0];
//							var textX = parseFloat($(node).attr("cx")) - 5.0;
//							var textY = parseFloat($(node).attr("cy")) - 13.0;
//							var field = $(node).attr("class").substring("point".length);
//							d3.select(node.parentNode).append("text")	// pointed day's caption
//								.attr("class", "caption")
//								.attr("x", textX+"%")
//								.attr("y", textY+"px")
//								.attr("fill", "green")
//								.text(function() {
//									var text = (self.values[i].config.value.type === 'percentage') ? PARAM.percentage(d[field]) : d[field];
//							    	return text;
//								});
//						}
//					}
//										
//					d3.selectAll("path[id*='arc']").filter(function(f) {		// arc related to the pointed day
//						return d.name === f.name;
//					}).attr("stroke", "green")
//					  .style("fill", general_config.similarity.fillSel);
//				})
//				.on("mouseout", function(d) {
//					d3.selectAll("circle").filter(function(f) {
//						return d.name === f.name;
//					}).attr("fill", general_config.similarity.fill)
//					  .attr("r", "4px");
//					
//					for(var i = 0; i < self.values.length; i++) {
//						for(var j = self.values[i].config.value.visualize[0]; j<= self.values[i].config.value.visualize[1]; j++) {
//							d3.selectAll(".point"+self.values[i].config.value.prefix+PARAM.indexdd(j)).filter(function(f) {
//								return d[self.values[i].config.value.prefix+PARAM.indexdd(j)] === f[self.values[i].config.value.prefix+PARAM.indexdd(j)];
//							}).attr("fill", general_config.similarity.fill)
//							  .attr("r", "4px")
//							  .style("cursor", "auto");
//						}
//						
//						d3.selectAll("path[id*='arc']").filter(function(f) {
//							return d.name === f.name;
//						}).style("fill", function(d) {
//							var t = d3.select(this).attr("id").split("arc");
//							return "url(\#" + t[0] + "radialGradient"+ t[1].substring(0,t[1].length-1) +")"; 
//						});
//					}
//					
//					// popover out
//					$(this).popover('destroy');
//					// ------
//					d3.selectAll(".caption").remove();
//					
//					d3.selectAll("[id*='arc']").filter(function(f) {
//						return d.name === f.name;
//					}).attr("stroke", general_config.similarity.stokeOn)
//					  .attr("stroke-width", "1");
//				})
//		circle.exit()
//			.remove();
//	};
//	
//	// Singleton - part II
//	AnalyticsPanel = function() {
//		return self;
//	};
//	
//	return this;	
//}
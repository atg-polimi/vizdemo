/**
 * 
 */
function RadialTree(config, primary, labeled) {
	
	this.config = config;
	this.data = {};
	
	var self = this;
	
	this.valueLegend = new ValueLegend(self, primary);
	this.analyticsPanel = new AnalyticsPanel(self, primary);
	
	var MAX_DEPTH = general_config.dataset.depth;
		
	var svg;
	var init = function() {
		general_config.radialTree.maxRadius = Math.max(PARAM.getRadius(self), general_config.radialTree.maxRadius);
		var radius = general_config.radialTree.maxRadius + general_config.radialTree.offset;
		svg = d3.select("#"+general_config.radialTree.id)
				.attr("width", radius*2)
				.attr("height", radius*2)
				.attr("transform", "translate(" + radius + "," + radius + ")");
		if (d3.select("defs")[0][0] == null)
			svg.append("defs");
		
		PARAM.YDomain(self);
		PARAM.YRange(self);
		PARAM.DYDomain(self);
		PARAM.DYRange(self);
	}();
	
	self.config.RadialTree.y = d3.scale.linear()
		.domain(self.config.RadialTree.YDomain)
		.range(self.config.RadialTree.YRange);
	
	self.config.RadialTree.dy = d3.scale.linear()
		.domain(self.config.RadialTree.DYDomain)
		.range(self.config.RadialTree.DYRange);
	
	var arc = d3.svg.arc()
		.startAngle(function(d) { return getStartAngle(d); })
		.endAngle(function(d) { return getEndAngle(d); })
		.innerRadius(function(d) { return self.config.RadialTree.y(d.y); })
		.outerRadius(function(d) { return self.config.RadialTree.dy(d.y+d.dy); });
	
	//Interpolate the scales
	var arcTween = function(d, e) {
		var xd = d3.interpolate(PARAM.x.domain(), [d.x, d.x + d.dx]),
			yr, dyr;

		if(d.depth === 0) {
			yr = d3.interpolate(self.config.RadialTree.y.range(), self.config.RadialTree.YRange);
			dyr = d3.interpolate(self.config.RadialTree.dy.range(), self.config.RadialTree.DYRange);
		} else {
//			var ranges = getYInterpolate(d.depth, self);
			var ranges = getYInterpolateBrutal(d.depth, self.config.RadialTree.prefix);
			yr =  d3.interpolate(self.config.RadialTree.y.range(), ranges.yr);
			dyr = d3.interpolate(self.config.RadialTree.dy.range(), ranges.dyr);
		}
		
		return function(d, i) {
			return i
	        	? function(t) { return arc(d); }
	        	: function(t) { 
	        		PARAM.x.domain(xd(t));
	        		self.config.RadialTree.y.range(yr(t));
	        		self.config.RadialTree.dy.range(dyr(t)); 
	        		
	    			recomputeGradient(e);
	        		
	        		return arc(d);
	        	};
		};
	}
	
	var click = 0;
	var timer = null;
	var similaritySelected = false;
	
	this.draw = function() {
		var partition = d3.layout.partition()
			.sort(null)		// A null comparator disables sorting and uses tree traversal order. [d3js.com]
			.value(function(d) { return 1; });
		
		var graph = svg.selectAll("path")
	      	.data(partition(self.data), function(d) { return self.config.RadialTree.prefix + self.config.RadialTree.count++; })
		  	.enter().append("g")
		  		.attr("id", function(d) { return "group"+ self.config.RadialTree.prefix + d.id; });
			
			var path = graph.append("path")
					.attr("d", arc)
					.attr("id", function(d) {
						return self.config.RadialTree.prefix + "arc" + d.id + "_";
					})
					.style("stroke", function(d) {
						if (d.depth === MAX_DEPTH)
							return null;
						return "#000000";
					})
					.style("stroke-width", function(d) {
//						if (d.depth === MAX_DEPTH)
//							return null;
						return 0.5;	
					})
					.style("fill", function(d, i) {
						if (!i) computeAggregateValues(d);		// executed only for the root element.
						return fillArc(d);
					})
					.on("mousemove", function(d) {
						ValueTooltip().show(d, d3.event.pageY, d3.event.pageX);
						lastMouseOverArc = d;
					})
					.on("mouseout", function(d) {
						ValueTooltip().remove();
						if (rtm_state === "similarity") {
							if ((! AnalyticsPanel.locked) && (! similaritySelected)) {
								if (d.depth == MAX_DEPTH) {
									dehighlightDays(new Analytics(d));
									$(".APbody").css("visibility", "hidden");
									$("#refday").html("------");
								}
							}
						}
						lastMouseOverArc = null;
						Log().add("mouseout", d);
					})
					.on("mouseover", function(d) {
						lastMouseOverArc = d;
						
						if (rtm_state === "similarity") {
							if (! similaritySelected) {
								if (! AnalyticsPanel.locked) {
									if (d.depth == MAX_DEPTH) {
										var matches = new Analytics(d);
										highlightDays(matches);
										self.analyticsPanel.draw({referenceDay: d, similarDays: matches.similarDays}, false);
									}
								}
							}
						}
						
						Log().add("mouseover", d);
					})
					.on("click", function(d) {
							click++;
							if (click == 1) {
								if (rtm_state === "similarity") similaritySelected = true;
								
								timer = setTimeout(function() {
									// perform single-click action
									if (rtm_state === "selection") {
										selectPreviousLayer(d);
									} else {
										if (d.depth == MAX_DEPTH) {
											if (! AnalyticsPanel.locked) {
												var matches = new Analytics(d);
												self.analyticsPanel.draw({referenceDay: d, similarDays: matches.similarDays}, true);
												similaritySelected = false;
												Log().add("setRefDay", AnalyticsPanel.referenceDay);
											}
										}
									}
									// ----
									click = 0;             //after action performed, reset counter
								}, 300);
							} else {
								clearTimeout(timer);    //prevent single-click action
								// perform double-click action
								self.timer = null;
								if (d.depth != MAX_DEPTH) {
									var visible = getVisibleElements(d);
									
									console.log("zoom() " + self.config.RadialTree.prefix);
									zoom(path, d, visible, true);
									caller(d, self);
									
									d3.selectAll(".visible").classed({'visible': false, 'notVisible': true});
									$.each(visible.id, function(i, v) {
										d3.select("[id*='arcLabel"+v+"']").classed({'visible': true, 'notVisible': false, 'arcLabelNormal': true, 'arcLabelSmall': false});
									});
									$.each(visible.parents, function(i, v) {
										d3.select("[id*='arcLabel"+v+"']").classed({'arcLabelNormal': false, 'arcLabelSmall': true});
									});
									
									general_config.extremesAtZoom = getZoomExtremes(d)
									general_config.selectionAtZoom = refinedSelection(general_config.extremesAtZoom);
									
									SelectionLegendPanel().clear();
									for(var i = 0; i < general_config.selectionAtZoom.length; i++)
										SelectionLegendPanel().addRow(general_config.selectionAtZoom[i]);
									
									HistogramPanelX().draw(general_config.selectionAtZoom);
								}
								// ----
								click = 0;             //after action performed, reset counter
							}
					})
					.on("dblclick", function(d){
						d3.event.preventDefault();  //cancel system double-click event
						if (d3.event.haveBeenCalled) {
							var visible = getVisibleElements(d);
							zoom(path,d, visible, false);
						}
					})
			  		.call(multipleSelector)		// drag 'n' drop
			  		.attr("class", function(d) {
			  			if (d.depth == MAX_DEPTH)
			  				return "deselected";
			  		});
			
			if (labeled) {
				graph.append("text")
					.attr("id", function(d) { return self.config.RadialTree.prefix + "arcLabel" + d.id; })
				  	.attr("x", 0)
				  	.attr("y", 0)
				  	.attr("dx", 8)
				  	.attr("dy", -4)
				  	.classed("arcLabelNormal visible", true)
				  	.style("stroke", "#000000")
				  	.append("svg:textPath")
				  		.attr("xlink:href", function(d) { return "#" + self.config.RadialTree.prefix + "arc" + d.id + "_"; })
				  		.text(function(d) { if (d.depth != MAX_DEPTH) return d.name; });
			}
	};
	
	var zoom = function(path, d, visible, direct) {
		if (direct) {
			var n = 0;
			path.transition()
				.duration(750)
				.attrTween("d", arcTween(d, visible.gradients))
				.each(function() {++n; })
				.each("end", function() {
					if (!--n) {
						SelectionMarker.remove('all');
						$.each(general_config.selectionAtZoom, function(i, v) {
							console.log(v);
							if (v.status === 'visible') new SelectionMarker(v);
						});
					}
				});
			Log().add("zoom", d);
		} else {
			path.transition()
				.duration(750)
				.attrTween("d", arcTween(d, visible.gradients));
		}
	}
	
	var computeAggregateValues = function(d) {
		if (d.children === undefined)
			return d;
		
		if (! d.aggregate)
			d.aggregate = {};
		
		for (var k = self.config.value.visualize[0]; k <= self.config.value.visualize[1]; k++)
			d.aggregate[self.config.value.prefix + PARAM.indexdd(k)] = 0.0;
		d.aggregate.leaves = 0;
		
		for (var i = 0; i < d.children.length; i++) {
			var aggregate = computeAggregateValues(d.children[i]);
			for (var j = self.config.value.visualize[0]; j <= self.config.value.visualize[1]; j++) {
				d.aggregate[self.config.value.prefix + PARAM.indexdd(j)] += aggregate[self.config.value.prefix + PARAM.indexdd(j)];
			}
			d.aggregate.leaves += aggregate.leaves || 1;
		}
		
		if (d.children[0].aggregate != undefined) {
			for(var k = self.config.value.visualize[0]; k <= self.config.value.visualize[1]; k++) {
				var avg = 0;
				for(var i = 0; i < d.children.length; i++) {
					avg += d.children[i].aggregate[self.config.value.prefix + PARAM.indexdd(k)] * d.children[i].aggregate.leaves;
				}
				d.aggregate[self.config.value.prefix + PARAM.indexdd(k)] = avg / d.aggregate.leaves;
			}
		} else {
			for(var k = self.config.value.visualize[0]; k <= self.config.value.visualize[1]; k++) {
				d.aggregate[self.config.value.prefix + PARAM.indexdd(k)] /= d.aggregate.leaves;
			}
		}
		
		return d.aggregate;
	};
	
	// Create the radialGradient element for the given "day".
	var fillArc = function(d) {
		var id = self.config.RadialTree.prefix + "radialGradient" + d.id;
		
		var cx = "0%";
		var cy = "0%";
		var r = getOuterRadius(d, self);
		var radialGradient = d3.select("defs").append("radialGradient")
			.attr("id", id)
			.attr("gradientUnits", "userSpaceOnUse")
			.attr("cx", cx)
			.attr("cy", cy)
			.attr("fx", cx)
			.attr("fy", cy)
			.attr("r", r);
		appendStopColors(radialGradient, d);
		
		return "url(\#" + id + ")";		
	};
	
	// Compute the right offsets and create the <stop> elements of the gradient.
	var appendStopColors = function(gradient, d) {
		var innerRad = self.config.RadialTree.y.range()[d.depth];
		var outerRad = self.config.RadialTree.dy.range()[d.depth];
		
		var scale = d3.scale.linear()
			.domain([0, 1])
			.range([innerRad, outerRad]);
		
		var output = d3.scale.linear()
			.domain([0, outerRad])
			.range([0, 1]);
		
		if (self.config.value.type === "absolute") {
			var m = d3.scale.linear()
				.domain([self.config.value.min, self.config.value.max])
				.range([0, 1]);
		}
		
		var cumulative = 0;
		var stopOffset = 0;
		var counter = 1;
		for (var i = self.config.value.visualize[0]; i <= self.config.value.visualize[1]; i++) {
			var value = (d.aggregate) ? d.aggregate : d;
			
			if (self.config.value.type === "absolute") { 
				stopOffset = output(scale(m(value[self.config.value.prefix + PARAM.indexdd(i)])));
			} else {
				cumulative += value[self.config.value.prefix + PARAM.indexdd(i)];
				stopOffset = output(scale(cumulative));
			}
			
			if (self.config.value.fill === "line") {
				gradient.append("stop")
					.attr("offset", stopOffset)
					.attr("stop-color", "white");
				gradient.append("stop")
					.attr("offset", stopOffset)
					.attr("stop-color", self.config.value.color[counter-1]);
				
				var stroke = (m) ? output(scale(m(value[self.config.value.prefix + PARAM.indexdd(i)] + 15))) : output(scale(cumulative+0.02));
				gradient.append("stop")
					.attr("offset", stroke)
					.attr("stop-color", self.config.value.color[counter-1]);
				gradient.append("stop")
					.attr("offset", stroke)
					.attr("stop-color", "white");
			} else {
				gradient.append("stop")
					.attr("offset", stopOffset)
					.attr("stop-color", self.config.value.color[counter-1]);
				
				if (self.config.value.color[counter])
					gradient.append("stop")
						.attr("offset", stopOffset)
						.attr("stop-color", self.config.value.color[counter]);
			}
			counter++;
		}
	};
	
	var recomputeGradient = function(g) {
		for(var i = 0; i < g.length; i++) {
			$(g[i]).empty();
			
			var gID = g[i].getAttribute("id").substring((self.config.RadialTree.prefix + "radialGradient").length);
			var d = d3.selectAll("#" + self.config.RadialTree.prefix + "arc" + gID + "_").datum();
			
			var gradient = d3.select(g[i]);
			gradient.attr("r", self.config.RadialTree.dy.range()[d.depth]);
			
			appendStopColors(gradient, d);
		}
	};
	
	var getVisibleElements = function(d) {
		var elements = [];
		// parents
		var p = d.parent;
		var parents = [];
		while(p) {
			parents.push(p.id);
			p = p.parent;
		}
		$.merge(elements, parents);
		
		// children (and element itself)
		$.merge(elements, getChildren(d));
		
		var gradients = [];
		$.each(elements, function(i, v) {
			gradients.push(d3.selectAll("[id$='" + self.config.RadialTree.prefix + "radialGradient"+v+"']")[0][0]);
		})
		
		return {gradients: gradients, id: elements, parents: parents};
	};
	
		
	// Return the ( max(selection)+1 ) index value, or 1 if the collection is empty.
	var getSelectionIndex = function() {
//		var max = Math.max.apply(Math, selection.map(function(o){ return o.index; })) + 1;
		if (PARAM.selections.length < 5) {
			var indexes = jsonPath(PARAM.selections, "$[*].index");
			
			var index = general_config.selection.label[0];
			for(var i = 0; i < general_config.selection.label.length; i++) {
				if ($.inArray(general_config.selection.label[i], indexes) == -1) return general_config.selection.label[i];
			}
			
			return index;
		}
		
		return 'aborted';
	}	
	
	var selectPreviousLayer = function(d) {
		allowed = (getSelectionIndex() === 'aborted') ? false : true;
		if (allowed) {
			if (d.children !== undefined) {
				var first = d;
				var last = d;
				while ((first.children !== undefined) || (last.children !== undefined)) {
					first = jsonPath(first.children, "$[0]")[0];
					last = jsonPath(last.children, "$[-1:]")[0];
				}
				
				var start = first.id;
				var end = last.id;
			
				new Selection(start, end, getSelectionIndex());
			}
		}
	}
	
	
	var selected = {start: 0, end: 0};
	var allowed = true;
	var multipleSelector = d3.behavior.drag().on("dragstart", function(d) {
		if (rtm_state === "selection") {
			allowed = (getSelectionIndex() === 'aborted') ? false : true;
			if (allowed) {
				if (d.depth === MAX_DEPTH) selected.start = d.id;
			}
		}
	})
	.on("drag", function(d) {
		if (rtm_state === "selection") {
			if ((d.depth === MAX_DEPTH) && (allowed)) {
				d3.selectAll("[id*='arc" + selected.start + "_']").attr("class", "onselection");
				
				var day = getSelectedArc(d.dx).id;
				
				if (day >= selected.start) {		// Selection is going clockwise
					d3.selectAll("[id*='arc']").filter(function(d) {
						if (d.depth === MAX_DEPTH) {
							return (d.id > selected.start) && (d.id <= day);
						}
						return false;
					}).attr("class", "onselection");	// Selected elements
					
					d3.selectAll("[id*='arc']").filter(function(d) {	// De-selected elements
						if (d.depth === MAX_DEPTH) {
							return (((d.id < selected.start) || (d.id > day)) && (this.getAttribute("class") != "selected"));
						}
						return false;
					}).attr("class", "deselected");
				} else {		// Selection is going anti-clockwise
					d3.selectAll("[id*='arc']").filter(function(d) {
						if (d.depth === MAX_DEPTH) {
							return (d.id < selected.start) && (d.id >= day);
						}
						return false;
					}).attr("class", "onselection");	// Selected elements
					
					d3.selectAll("[id*='arc']").filter(function(d) {
						if (d.depth === MAX_DEPTH) {
							return (((d.id > selected.start) || (d.id < day)) && (this.getAttribute("class") != "selected"));
						}
						return false;
					}).attr("class", "deselected");		// De-selected elements
				}
			}
		}
	})
	.on("dragend", function(d) {
		if (rtm_state === "selection") {
			if ((d.depth === MAX_DEPTH) && (allowed)) {
				selected.end = getSelectedArc(self, d.dx).id;
				
				if (selected.start > selected.end) {
					var temp = selected.start;
					selected.start = selected.end;
					selected.end = temp;
				}
				
				new Selection(selected.start, selected.end, getSelectionIndex());
				selected = {start: 0, end: 0};
			}
		}
	});
		
}
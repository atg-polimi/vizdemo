/**
 * 
 */
var PARAM = {
		mouse: {
			x: 0,
			y: 0
		},
		scroll: {
			top: 0,
			left: 0
		},
		x: d3.scale.linear().range([0, general_config.radialTree.RadialBound]),
		indexdd : function(index) {
			var f = d3.format("02d");
			return f(index);
		},
		percentage : function(index) {
			var f = d3.format(".2%");
			return f(index);
		},
		twodecimal : function(index) {
			var f = d3.format(".2");
			return f(index);
		},		
		getRadius : function(rtm) {
			var radius = 0;
			$.each(rtm.config.RadialTree.LAYER_SPACING, function(i, value) { radius += value; });
			$.each(rtm.config.RadialTree.LAYER_WIDTH, function(i, value) { radius += value; });
			return radius;
		},
		selections : [],
		getSelectionCopy : function() {
			return $.extend(true, [], PARAM.selections);
		},
		YDomain : function(rtm) {
			var length = general_config.dataset.depth + 1;
			var result = [];
			for(var i = 0; i < length; i++)
				result.push((1/length)*i);
			
			rtm.config.RadialTree['YDomain'] = result;
			return result;
		},
		
		YRange : function(rtm) {
			var length = general_config.dataset.depth + 1;
			var result = [rtm.config.RadialTree.LAYER_SPACING[0]];
			for(var i = 1; i < length; i++) {
				var value = result[i-1] + rtm.config.RadialTree.LAYER_WIDTH[i-1] + rtm.config.RadialTree.LAYER_SPACING[i];
				result.push(value);
			}
			
			rtm.config.RadialTree['YRange'] = result;
			return result;
		},
		DYDomain : function(rtm) {
			var length = general_config.dataset.depth + 1;
			var result = [];
			for(var i = 0; i < length; i++)
				result.push((1/length)*(i+1));
		
			rtm.config.RadialTree['DYDomain'] = result;
			return result;
		},
		DYRange : function(rtm) {
			var length = general_config.dataset.depth + 1;
			var result = [rtm.config.RadialTree.LAYER_SPACING[0]+rtm.config.RadialTree.LAYER_WIDTH[0]];
			for(var i = 1; i < length; i++) {
				var value = result[i-1] + rtm.config.RadialTree.LAYER_SPACING[i] + rtm.config.RadialTree.LAYER_WIDTH[i];
				result.push(value);
			}
		
			rtm.config.RadialTree['DYRange'] = result;
			return result;
		}
}
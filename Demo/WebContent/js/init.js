

var data_structure,
	rtm_state,
	rtm,
	rtm_2,
	instances;

var initialTransform;
var lastMouseOverArc = null;

window.onload = function() {
	new Log();
	
	rtm = new RadialTree(config_1, true);
	rtm_2 = new RadialTree(config_2, false, true);
	instances = [rtm, rtm_2];
	
	new ValueTooltip(instances);
	new SelectionLegendPanel();
	new HistogramPanelX(instances);
//	new AnalyticsPanel(instances);

	d3.select(document)
	.on("keydown", function() {
		if (d3.event.shiftKey) {
			if (lastMouseOverArc != null) {
				if (lastMouseOverArc.depth === general_config.dataset.depth)
					showMicro(lastMouseOverArc.id, lastMouseOverArc.name);
			}
		}
	});
	
	initialTransform = d3.select("#"+general_config.radialTree.id).attr("transform");
	// Load dataset and draw the chart.
	$.ajax({
		url: "http://alpine-carrier-727.appspot.com/getRadialTreeGeneral_02",
		type: 'GET',
		cache: true,
		async: false,
		crossDomain: true,
		dataType: "json",
		success: function (data) {
			data_structure = data;
			
			rtm.data = data_structure;
			rtm.draw();
			
			rtm_2.data = data_structure;
			rtm_2.draw();
			
			
		},
		error:function(data, status, error) {
			console.log("error: "+ data + " status: " + status + " er:" + error);
		}
	});
	
	// Intercept the mouse position.
	$(document).mousemove(function(e) {
		PARAM.mouse.x = e.pageX;
		PARAM.mouse.y = e.pageY;
	});
	// Get the scroll offset
	$(document).scroll(function(e) {
		PARAM.scroll.top = $(this).scrollTop();
		PARAM.scroll.left = $(this).scrollLeft();
	});
	
//	 Log mouse position
	setInterval(function() {
		var view = "";
		if (d3.select("#macro").style("display") == "none") {
			view = "detailed";
		} else {
			view = "overview";
		}
		
		Log().add("mouse", [PARAM.mouse.x, PARAM.mouse.y], view);
	}, 100);
	
}
/**
 * 
 */

function Selection(start, end, index) {

	if (!(this instanceof Selection)) {
		return new Selection();
	}
	
	var selection = {};
	selection.start = start;
	selection.end = end;
	selection.index = index;
	selection.color = index;
	selection.status = 'visible';
	
	// add & show the selection
	var collection = PARAM.selections;
	collection.push(selection);	// Logical selection
	
	// Visual selection
	d3.selectAll("[id*='arc']").filter(function(d) {
		if (d.depth === general_config.dataset.depth)
			return (d.id >= selection.start) && (d.id <= selection.end);
		return false
	}).attr("class", "selected");
	
	var marker = new SelectionMarker(selection); 		// add selection marker
	var legend = new SelectionLegendPanel().addRow(selection);	// add row to the legend
	new HistogramPanelX().draw(collection);	// Update histograms 
	Selection.refeshAll(collection);
	
	Log().add("selection", [selection.start, selection.end]);
	// ------------------
	
	Selection.prototype.remove = Selection.remove;
	Selection.prototype.edit = Selection.edit;
	Selection.prototype.removeAll = Selection.removeAll;
	
	return this;
}

Selection.remove = function(index) {
	var collection = PARAM.selections;
		toBeRemoved = jsonPath(collection, "$[?(@.index=='" + index + "')]")[0]	// object
	
	// Visual deselection - part I
	d3.selectAll("[id*='arc']").filter(function(d) {
		return (d.id >= toBeRemoved.start) && (d.id <= toBeRemoved.end);
	}).attr("class", "deselected");
		
	collection.splice(collection.indexOf(toBeRemoved), 1);	// Logical deselection
	Selection.refeshAll(collection);						// Visual deselection - part II
	
	SelectionMarker.remove(index);							// remove selection marker
	var legend = new SelectionLegendPanel().removeRow(index);		// remove row from legend
	if (! general_config.selectionAtZoom) {
		HistogramPanelX().draw(PARAM.selections);		// Update histograms
	} else {
		general_config.selectionAtZoom = refinedSelection(general_config.extremesAtZoom); 
		HistogramPanelX().draw(general_config.selectionAtZoom)		// Update histograms
	}
}

Selection.removeAll = function() {
	var collection = PARAM.selections;
	var indexes = jsonPath(collection, "$.[*].index")	// object
	
	for (var i = 0; i < indexes.length; i++) 
		Selection.remove(indexes[i]);
	
	new SelectionLegendPanel().clear();
};

Selection.refeshAll = function(collection) {	// Restore overlapping selections
	for(var i = 0; i < collection.length; i ++) {
		d3.selectAll("[id*='arc']").filter(function(d) {
			return (d.id >= collection[i].start) && (d.id <= collection[i].end);
		}).attr("class", "selected");
	}
};

Selection.edit = function(index, date, interval) {	// Modify logically and visually a selection
	var day = moment(date);
	var toEdit = jsonPath(PARAM.selections, "$[?(@.index=='" + index + "')]")[0];
	
	var arcs = getSelectionArcs(toEdit);	// Visual edit - part I
	arcs.attr("class", "deselected");		// ""

	var edit = d3.selectAll("path[id*='arc']").filter(function(f) {	// Logical edit
		return f.name == day.format(general_config.date.formatIn);
	});
	
	if (interval === "start"){
		toEdit.start = edit.datum().id;
	} else if (interval === "end") {
		toEdit.end = edit.datum().id;
	}

	arcs = getSelectionArcs(toEdit);		// Visual edit - part II
	arcs.attr("class", "selected");			// ""

	if (! general_config.selectionAtZoom) {
		SelectionMarker.remove(index);		// marker repositioning
		//console.log(index);
		new SelectionMarker(toEdit);		// 		""
		HistogramPanelX().draw(PARAM.selections);		// Update histograms
	} else {
		SelectionMarker.remove(index);		// marker repositioning
		general_config.selectionAtZoom = refinedSelection(general_config.extremesAtZoom);

		SelectionLegendPanel().clear();
		for (var i = 0; i < general_config.selectionAtZoom.length; i++) {
			SelectionLegendPanel().addRow(general_config.selectionAtZoom[i]);
			if ((Math.floor(general_config.selectionAtZoom[i].index) === index) && (general_config.selectionAtZoom[i].status === 'visible'))
				new SelectionMarker(general_config.selectionAtZoom[i]);
		}
		HistogramPanelX().draw(general_config.selectionAtZoom);		// Update histograms
	}
	
	Log().add("editSelection", toEdit);
};
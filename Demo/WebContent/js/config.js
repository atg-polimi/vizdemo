/**
 * 
 */
var config_1 = 
{
	"value": {
		"prefix": "value",
		"type": "percentage",
		"visualize": [1, 4],
		"fill": "fill",
		"id": ["bedroom", "kitchen", "livingroom", "toilet"],
		"label": ["Bedroom", "Kitchen", "Living room", "Toilet"],
		//"color": ["RGBA(52,18,139,1)", "RGBA(229,86,69,1)", "RGBA(12,149,12,1)", "RGBA(160,203,239,1)"],
		"color": ["RGBA(153,140,186,1)", "RGBA(109,52,69,1)", "RGBA(98,163,98,1)", "RGBA(57,58,58,1)"],
	},
	"RadialTree": {
		"prefix": "RT1",
		"count": 0,
		"LAYER_SPACING": [20, 52, 52, 52],
		"LAYER_WIDTH": [50, 50, 50, 50],
		"Tween_LAYERS_SPACE": 30,
		"Tween_LAYERS_WIDTH": 10
	}
};

var config_2 = 
{
	"value": {
		"prefix": "value",
		"type": "absolute",
		"visualize": [5, 6],
		"min": 0,
		"max": 220,
		"fill": "line",
		"id": ["bpMin", "bpMax"],
		"label": ["bp Min", "bp Max"],
		"color": ["RGBA(111,111,255,1)", "RGBA(255,76,76,1)"],
	},
	"RadialTree": {
		"prefix": "RT2",
		"count": 0,
		"LAYER_SPACING": [72, 72, 72, 72],
		"LAYER_WIDTH": [30, 30, 30, 30],
		"Tween_LAYERS_SPACE": 30,
		"Tween_LAYERS_WIDTH": 10
	}
};